import 'package:dartz/dartz.dart';
import 'package:drclick_aliados/app/modules/login/domain/entities/user_api_entity.dart';
import 'package:drclick_aliados/app/modules/login/domain/entities/usuario_entity.dart';
import 'package:drclick_aliados/app/modules/login/domain/repository/login_repository.dart';
import 'package:drclick_aliados/app/modules/login/domain/usecases/login_usecase.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:mockito/mockito.dart';

class MockLoginRepository extends Mock implements LoginRepository {}

void main() {
  MockLoginRepository mockLoginRepository;
  LoginSistema usecase;
  UserApiTokenEntity tokenEntity;

  setUp(() {
    mockLoginRepository = MockLoginRepository();
    usecase = LoginSistema(repository: mockLoginRepository);
    tokenEntity = UserApiTokenEntity(
      accessToken: 'test',
      clientId: 'test',
      refreshToken: 'test',
      scope: 'test',
      tokenType: 'test',
      usuario: UsuarioEntity(
          password: 'test',
          username: 'test',
          enabled: true,
          idUsuario: 123,
          img: 'test',
          google: true,
          uid: null,
          persona: null),
    );
  });

  test(
    'debe retornar el tokenEntity cuando las credenciales sean correctas',
    () async {
      //arrange
      when(mockLoginRepository.login(any, any))
          .thenAnswer((_) async => Right(tokenEntity));
      //act
      final res =
          await usecase(ParamsLogin(username: 'test', password: 'test'));

      //assert
      expect(res, Right(tokenEntity));
      verify(mockLoginRepository.login('test', 'test'));
    },
  );
}
