import 'package:drclick_aliados/app/modules/login/data/models/persona_model.dart';
import 'package:drclick_aliados/app/modules/login/data/models/tipo_documento_model.dart';
import 'package:drclick_aliados/app/modules/login/data/models/user_api_model.dart';
import 'package:drclick_aliados/app/modules/login/domain/entities/tipo_documento_entity.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  UserApiTokenModel userApiModel;
  TipoDocumentoModel docModel;
  PersonaModel personaModel;

  setUp(() {
    docModel = TipoDocumentoModel(
      idTipoDocumento: 1,
      descripcion: "test",
      estado: true,
      abreviatura: "test",
    );
  });
  group('tipoDocumentoModel', () {
    test(
      'debe ser una subclase de TipoDocumentoEntity',
      () async {
        expect(docModel, isA<TipoDocumentoEntity>());
      },
    );
  });
}
