import 'package:dartz/dartz.dart';
import 'package:drclick_aliados/app/core/shared/errors/exceptions.dart';
import 'package:drclick_aliados/app/core/shared/errors/failures.dart';
import 'package:drclick_aliados/app/modules/login/data/datasources/remote_datasource.dart';
import 'package:drclick_aliados/app/modules/login/data/models/user_api_model.dart';
import 'package:drclick_aliados/app/modules/login/data/repositories/login_repository_impl.dart';
import 'package:drclick_aliados/app/modules/login/domain/entities/user_api_entity.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockRemotaDataSource extends Mock implements LoginRemoteDataSource {}

void main() {
  MockRemotaDataSource dataSource;
  LoginRepositoryImpl repository;
  UserApiTokenModel token;
  UserApiTokenEntity tokenEntity;
  setUp(() {
    dataSource = MockRemotaDataSource();
    repository = LoginRepositoryImpl(dataSource: dataSource);
    token = UserApiTokenModel(
      accessToken: 'test',
      clientId: 'test',
      refreshToken: 'test',
      scope: 'test',
      tokenType: 'test',
      usuario: null,
    );
    tokenEntity = token;
  });

  test(
    'debe retornar un tokenEntityModel cuando el login es correcto',
    () async {
      //arrange
      when(dataSource.login(any, any))
          .thenAnswer((realInvocation) async => tokenEntity);

      //act
      final res = await repository.login('test', 'test');

      //assert
      expect(res, Right(tokenEntity));
      verify(dataSource.login('test', 'test'));
    },
  );

  test(
    'debe retornar un ServerFailure cuando el login es incorrecto',
    () async {
      //arrange
      when(dataSource.login(any, any)).thenThrow(ServerException());

      //act
      final res = await repository.login('test', 'test');

      //assert
      expect(res, Left(ServerFailure()));
      verify(dataSource.login('test', 'test'));
    },
  );
}
