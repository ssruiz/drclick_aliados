import 'package:flutter/material.dart';

final Map<String, Color> coloresSnack = {
  'error': Colors.red[700],
  'success': Colors.green[700]
};
