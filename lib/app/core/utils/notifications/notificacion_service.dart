import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

import '../../../config/themes/fonts.dart';
import 'notificacion_colors.dart';

class NotificationService {
  void mostrarSnackBar(
      {@required String color,
      @required String mensaje,
      @required String titulo,
      SnackPosition position = SnackPosition.TOP}) {
    Get.snackbar(
      '',
      '',
      snackPosition: position,
      titleText: Text(
        titulo,
        style: AppFonts.primaryFont.copyWith(
          fontSize: 16,
          color: Colors.white,
        ),
      ),
      messageText: Text(
        mensaje,
        style: AppFonts.primaryFont.copyWith(
          fontSize: 15,
          color: Colors.white,
        ),
      ),
      colorText: Colors.white,
      backgroundColor: coloresSnack[color],
      duration: Duration(
        milliseconds: 2500,
      ),
    );
  }
}
