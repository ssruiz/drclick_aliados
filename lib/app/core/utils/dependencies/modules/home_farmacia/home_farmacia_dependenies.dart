import 'package:get/get.dart';

import '../../../../../modules/home_farmacia/data/datasources/local_data_source.dart';
import '../../../../../modules/home_farmacia/data/repositories/home_farmacia_repository_impl.dart';
import '../../../../../modules/home_farmacia/domain/repository/home_farmacia_repository.dart';
import '../../../../../modules/home_farmacia/domain/usecases/logout_use_case.dart';
import '../../../../authentication/domain/usecases/delete_auth_token.dart';

class HomeFarmaciaDependencies {
  static void inject() {
    // UseCase
    Get.lazyPut(
      () => CerrarSesion(
        repository: Get.find(),
      ),
      fenix: true,
    );

    // Repository
    Get.lazyPut<HomeFarmaciaRepository>(
      () => HomeFarmaciaRepositoryImpl(
        dataSource: Get.find<HomeFarmaciaLocalDataSource>(),
      ),
      fenix: true,
    );
    //DataSource
    Get.lazyPut<HomeFarmaciaLocalDataSource>(
      () => HomeFarmaciaLocalDataSourceImpl(
        deleteAuthToken: Get.find<DeleteAuthToken>(),
      ),
      fenix: true,
    );
  }
}
