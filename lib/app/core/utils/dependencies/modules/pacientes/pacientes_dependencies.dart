import 'package:get/get.dart';

import '../../../../../modules/pacientes/data/datasources/remote_data_source.dart';
import '../../../../../modules/pacientes/data/repositories/paciente_repository_impl.dart';
import '../../../../../modules/pacientes/domain/repository/pacientes_repository.dart';
import '../../../../../modules/pacientes/domain/usecases/buscar_paciente_ci.dart';
import '../../../../../modules/pacientes/domain/usecases/verificar_paciente.dart';
import '../../../../shared/dio/dio_config.dart';

class PacientesDependencies {
  static void inject() {
    // UseCases
    Get.lazyPut(
      () => BuscarPacienteCi(
        repository: Get.find(),
      ),
      fenix: true,
    );

    Get.lazyPut(
      () => VerificarPaciente(
        repository: Get.find(),
      ),
      fenix: true,
    );
    // Repository
    Get.lazyPut<PacientesRepository>(
      () => PacientesRepositoryImpl(
        remoteDataSource: Get.find<PacienteRemoteDataSource>(),
      ),
      fenix: true,
    );
    //DataSource
    Get.lazyPut<PacienteRemoteDataSource>(
      () => PacienteRemoteDataSourceImpl(
        dataSource: Get.find<DioService>(),
      ),
      fenix: true,
    );
  }
}
