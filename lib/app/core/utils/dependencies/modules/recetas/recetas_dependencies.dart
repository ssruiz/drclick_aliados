import 'package:get/get.dart';

import '../../../../../modules/recetas/data/datasources/receta_local_data_source.dart';
import '../../../../../modules/recetas/data/datasources/receta_remote_data_source.dart';
import '../../../../../modules/recetas/data/repositories/receta_repository_impl.dart';
import '../../../../../modules/recetas/domain/repository/recetas_repository.dart';
import '../../../../../modules/recetas/domain/usecases/buscar_recetas_ci_usecase.dart';
import '../../../../../modules/recetas/domain/usecases/buscar_recetas_codigo_usecase.dart';
import '../../../../../modules/recetas/domain/usecases/scan_codigo_usecase.dart';
import '../../../../../modules/recetas/domain/usecases/verificar_receta_usecase.dart';
import '../../../../shared/dio/dio_config.dart';

class RecetasDependencies {
  static void inject() {
    // UseCases
    Get.lazyPut(
      () => BuscarRecetasCi(
        repository: Get.find(),
      ),
      fenix: true,
    );

    Get.lazyPut(
      () => BuscarRecetasCodigo(
        repository: Get.find(),
      ),
      fenix: true,
    );

    Get.lazyPut(
      () => ScanCodigo(
        repository: Get.find(),
      ),
      fenix: true,
    );

    Get.lazyPut(
      () => VerificarReceta(
        repository: Get.find(),
      ),
      fenix: true,
    );
    // Repository
    Get.lazyPut<RecetasRepository>(
      () => RecetaRepositoryImpl(
        localDataSource: Get.find<RecetaLocalDataSource>(),
        remoteDataSource: Get.find<RecetaRemoteDataSource>(),
      ),
      fenix: true,
    );

    //DataSource
    Get.lazyPut<RecetaLocalDataSource>(
      () => RecetaLocalDataSourceImpl(),
      fenix: true,
    );

    Get.lazyPut<RecetaRemoteDataSource>(
      () => RecetaRemoteDataSourceImpl(
        dataSource: Get.find<DioService>(),
      ),
      fenix: true,
    );
  }
}
