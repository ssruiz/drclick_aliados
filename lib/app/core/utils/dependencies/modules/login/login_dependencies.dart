import 'package:get/get.dart';

import '../../../../../modules/login/data/datasources/remote_datasource.dart';
import '../../../../../modules/login/data/repositories/login_repository_impl.dart';
import '../../../../../modules/login/domain/repository/login_repository.dart';
import '../../../../../modules/login/domain/usecases/login_usecase.dart';
import '../../../../../modules/login/domain/usecases/restaurar_password_usecase.dart';
import '../../../../shared/dio/dio_config.dart';

class LoginDependencies {
  static void inject() {
    Get.lazyPut(
      () => LoginSistema(
        repository: Get.find(),
      ),
      fenix: true,
    );

    Get.lazyPut(
      () => RestaurarPassword(
        repository: Get.find(),
      ),
      fenix: true,
    );

    // Repository
    Get.lazyPut<LoginRepository>(
      () => LoginRepositoryImpl(
        dataSource: Get.find<LoginRemoteDataSource>(),
      ),
      fenix: true,
    );
    //DataSource
    Get.lazyPut<LoginRemoteDataSource>(
      () => LoginRemoteDataSourceDio(
        dataSource: Get.find<DioService>(),
      ),
      fenix: true,
    );
  }
}
