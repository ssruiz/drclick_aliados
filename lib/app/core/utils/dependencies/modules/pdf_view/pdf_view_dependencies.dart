import 'package:drclick_aliados/app/modules/pdfView/data/datasources/local_data_source.dart';
import 'package:drclick_aliados/app/modules/pdfView/data/datasources/remote_data_source.dart';
import 'package:drclick_aliados/app/modules/pdfView/data/repositories/pdf_view_repository_impl.dart';
import 'package:drclick_aliados/app/modules/pdfView/domain/repository/pdf_view_repository.dart';
import 'package:drclick_aliados/app/modules/pdfView/domain/usecases/check_file..dart';
import 'package:drclick_aliados/app/modules/pdfView/domain/usecases/get_from_url.dart';
import 'package:get/get.dart';

import '../../../../../modules/pacientes/data/datasources/remote_data_source.dart';
import '../../../../../modules/pacientes/data/repositories/paciente_repository_impl.dart';
import '../../../../../modules/pacientes/domain/repository/pacientes_repository.dart';
import '../../../../../modules/pacientes/domain/usecases/buscar_paciente_ci.dart';
import '../../../../../modules/pacientes/domain/usecases/verificar_paciente.dart';
import '../../../../shared/dio/dio_config.dart';

class PdfViewDependencies {
  static void inject() {
    // UseCases
    Get.lazyPut(
      () => CheckFile(
        repository: Get.find(),
      ),
      fenix: true,
    );

    Get.lazyPut(
      () => GetPdfFromUrl(
        repository: Get.find(),
      ),
      fenix: true,
    );
    // Repository
    Get.lazyPut<PdfViewRepository>(
      () => PdfViewRepositoryImpl(
        remoteDataSource: Get.find<PdfViewRemoteDataSource>(),
        localDataSource: Get.find<PdfViewLocalDataSource>(),
      ),
      fenix: true,
    );
    //DataSource
    Get.lazyPut<PdfViewRemoteDataSource>(
      () => PdfViewRemoteDataSourceImpl(
        dataSource: Get.find<DioService>(),
      ),
      fenix: true,
    );

    Get.lazyPut<PdfViewLocalDataSource>(
      () => PdfViewLocalDataSourceImpl(),
      fenix: true,
    );
  }
}
