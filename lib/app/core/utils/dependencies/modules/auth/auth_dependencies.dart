import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../authentication/data/datasources/local_data_source.dart';
import '../../../../authentication/data/repositories/auth_repository_impl.dart';
import '../../../../authentication/domain/repositories/auth_repository.dart';
import '../../../../authentication/domain/usecases/delete_auth_token.dart';
import '../../../../authentication/domain/usecases/get_auth_token.dart';
import '../../../../authentication/domain/usecases/set_auth_token.dart';
import '../../../../authentication/domain/usecases/verificar_credenciales.dart';

class AuthDependencies {
  static void inject() {
    // Use Cases
    Get.lazyPut(
      () => SetAuthToken(
        repository: Get.find(),
      ),
      fenix: true,
    );
    Get.lazyPut(
      () => GetAuthToken(
        repository: Get.find(),
      ),
      fenix: true,
    );
    Get.lazyPut(
      () => DeleteAuthToken(
        repository: Get.find(),
      ),
      fenix: true,
    );
    Get.lazyPut(
      () => VerificarCredenciales(
        repository: Get.find(),
      ),
      fenix: true,
    );

    // Repository

    Get.lazyPut<AuthRepository>(
      () => AuthRepositoryImpl(
        localDataSource: Get.find<AuthLocalDataSource>(),
      ),
      fenix: true,
    );

    //DataSource
    Get.lazyPut<AuthLocalDataSource>(
      () => AuthLocalDataSourceImpl(
        sharedPreferences: Get.find<SharedPreferences>(),
      ),
      fenix: true,
    );
  }
}
