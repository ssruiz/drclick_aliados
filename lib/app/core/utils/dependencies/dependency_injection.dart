import 'package:drclick_aliados/app/core/utils/dependencies/modules/pdf_view/pdf_view_dependencies.dart';
import 'package:drclick_aliados/app/core/utils/dependencies/modules/recetas/recetas_dependencies.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../config/routes/navigator.dart';
import '../../shared/dio/dio_config.dart';
import '../logger/logger_service.dart';
import '../notifications/notificacion_service.dart';
import 'modules/auth/auth_dependencies.dart';
import 'modules/home_farmacia/home_farmacia_dependenies.dart';
import 'modules/login/login_dependencies.dart';
import 'modules/pacientes/pacientes_dependencies.dart';

class DependencyInjection {
  static Future<void> init() async {
    // App dependencies

    // ---------  Dio
    Get.lazyPut<DioService>(
      () => DioService(),
      fenix: true,
    );
    // --------- Modulos

    // ---------  --------- Auth
    AuthDependencies.inject();

    //  --------- --------- Login
    LoginDependencies.inject();

    //  --------- --------- HomeFarmacia
    HomeFarmaciaDependencies.inject();

    //  --------- --------- Pacientes
    PacientesDependencies.inject();

    //  --------- --------- Pacientes
    RecetasDependencies.inject();

    //  --------- --------- Pacientes
    PdfViewDependencies.inject();
    //  External

    //  --------- Shared Preferences

    final shared = await SharedPreferences.getInstance();
    Get.put<SharedPreferences>(shared);

    //  Util
    //  --------- Navigation

    Get.put<NavigatorController>(NavigatorController());

    //  --------- Notifications
    Get.put<NotificationService>(NotificationService());

    //  --------- Logger
    Get.put<LoggerService>(LoggerService());
  }
}
