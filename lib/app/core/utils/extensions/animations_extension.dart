import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';
import 'package:supercharged/supercharged.dart';

extension AnimationsExtension on Widget {
  Widget animateScale({int duration = 800, int delay = 0}) {
    return PlayAnimation<double>(
      tween: (0.0).tweenTo(1.0),
      delay: Duration(milliseconds: delay),
      duration: Duration(milliseconds: duration),
      curve: Curves.bounceInOut,
      builder: (context, child, value) {
        return Transform.scale(
          alignment: Alignment.center,
          scale: value,
          child: this,
        );
      },
    );
  }

  Widget animateBouncing({int duration = 1000}) {
    return MirrorAnimation<double>(
      tween: (0.0).tweenTo(25.0),
      duration: Duration(milliseconds: duration),
      curve: Curves.easeIn,
      child: this,
      builder: (context, child, value) {
        return Transform.translate(
          offset: Offset(0, value),
          child: child,
        );
      },
    );
  }
}
