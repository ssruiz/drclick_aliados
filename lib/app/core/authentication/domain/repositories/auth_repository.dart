import 'package:dartz/dartz.dart';

import '../../../shared/errors/failures.dart';
import '../entities/token_entity.dart';

abstract class AuthRepository {
  Future<Either<Failure, TokenEntity>> getAuthToken();
  Future<Either<Failure, TokenEntity>> setAuthToken(
      String username, String email, String token, String authority);
  Future<Either<Failure, bool>> deleteAuthToken();

  Either<Failure, bool> verificarCredenciales(String authority);
}
