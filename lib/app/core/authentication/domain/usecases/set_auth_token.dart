import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../shared/errors/failures.dart';
import '../../../shared/usecases/usecase_params.dart';
import '../entities/token_entity.dart';
import '../repositories/auth_repository.dart';

class SetAuthToken implements UseCaseWithParams<TokenEntity, SetTokenParams> {
  final AuthRepository repository;

  SetAuthToken({@required this.repository});

  @override
  Future<Either<Failure, TokenEntity>> call(SetTokenParams params) {
    return repository.setAuthToken(
        params.username, params.email, params.token, params.authority);
  }
}

class SetTokenParams {
  final String username;
  final String email;
  final String token;
  final String authority;
  SetTokenParams({
    @required this.username,
    @required this.email,
    this.token,
    this.authority,
  });
}
