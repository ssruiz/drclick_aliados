import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../shared/errors/failures.dart';
import '../../../shared/usecases/usecase.dart';
import '../entities/token_entity.dart';
import '../repositories/auth_repository.dart';

class GetAuthToken implements UseCase<TokenEntity> {
  final AuthRepository repository;

  GetAuthToken({@required this.repository});

  @override
  Future<Either<Failure, TokenEntity>> call() async {
    return await repository.getAuthToken();
  }
}
