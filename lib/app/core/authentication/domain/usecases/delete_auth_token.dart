import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../shared/errors/failures.dart';
import '../../../shared/usecases/usecase.dart';
import '../repositories/auth_repository.dart';

class DeleteAuthToken implements UseCase<bool> {
  final AuthRepository repository;

  DeleteAuthToken({@required this.repository});

  @override
  Future<Either<Failure, bool>> call() {
    return repository.deleteAuthToken();
  }
}
