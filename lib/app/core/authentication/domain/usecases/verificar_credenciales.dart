import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../shared/errors/failures.dart';
import '../../../shared/usecases/usecase_params_sync.dart';
import '../repositories/auth_repository.dart';

class VerificarCredenciales implements UseCaseWithParamsSync<bool, String> {
  final AuthRepository repository;

  VerificarCredenciales({@required this.repository});

  @override
  Either<Failure, bool> call(String params) {
    return repository.verificarCredenciales(params);
  }
}
