import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class TokenEntity extends Equatable {
  final String username;
  final String email;
  final String token;
  final String authority;
  TokenEntity({
    @required this.username,
    @required this.email,
    this.token,
    this.authority,
  });

  @override
  List<Object> get props => [username, email, token, authority];
}
