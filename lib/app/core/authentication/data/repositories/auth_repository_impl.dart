import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../shared/errors/exceptions.dart';
import '../../../shared/errors/failures.dart';
import '../../domain/entities/token_entity.dart';
import '../../domain/repositories/auth_repository.dart';
import '../datasources/local_data_source.dart';
import '../models/usuario_token_model.dart';

class AuthRepositoryImpl implements AuthRepository {
  final AuthLocalDataSource localDataSource;

  AuthRepositoryImpl({@required this.localDataSource});

  @override
  Future<Either<Failure, bool>> deleteAuthToken() async {
    try {
      final result = await localDataSource.deleteAuthToken();
      return Right(result);
    } on CacheException {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, TokenEntity>> getAuthToken() async {
    try {
      final result = await localDataSource.getAuthToken();
      return Right(result);
    } on CacheException {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, TokenEntity>> setAuthToken(
      String username, String email, String token, String authority) async {
    try {
      final result = await localDataSource.setAuthToken(
        TokenModel(
          username: username,
          email: email,
          token: token,
          authority: authority,
        ),
      );
      return Right(result);
    } on CacheException {
      return Left(CacheFailure());
    }
  }

  @override
  Either<Failure, bool> verificarCredenciales(String authority) {
    try {
      return Right(localDataSource.verificarCredenciales(authority));
    } on AuthException {
      return Left(AuthFailure());
    }
  }
}
