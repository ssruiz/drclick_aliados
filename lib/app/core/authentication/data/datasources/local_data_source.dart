import 'dart:convert';

import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../shared/errors/exceptions.dart';
import '../models/usuario_token_model.dart';

abstract class AuthLocalDataSource {
  /// Gets the cached [TokenModel] from the shared preferences
  ///
  /// Throws [CacheExpcetion] if there is not cached data or when something went wrong
  Future<TokenModel> getAuthToken();

  /// Caches the [TokenModel] in share preferences
  ///
  /// Throws [CacheExpcetion] when something went wrong
  Future<TokenModel> setAuthToken(TokenModel tokenModel);

  /// Clear the chaes
  ///
  /// Throws [CacheExpcetion] when something went wrong
  Future<bool> deleteAuthToken();

  /// Verifica las credenciales del usuario
  ///
  /// Throws [AuthException] when something went wrong
  bool verificarCredenciales(String authority);
}

const CACHED_TOKEN = "CACHED_TOKEN";

class AuthLocalDataSourceImpl implements AuthLocalDataSource {
  final SharedPreferences sharedPreferences;

  AuthLocalDataSourceImpl({@required this.sharedPreferences});

  @override
  Future<bool> deleteAuthToken() async {
    final jsonString = await sharedPreferences.remove(CACHED_TOKEN);
    if (jsonString == false) throw CacheException();
    return Future.value(true);
  }

  @override
  Future<TokenModel> getAuthToken() {
    final jsonString = sharedPreferences.getString(CACHED_TOKEN);
    if (jsonString == null) throw CacheException();
    return Future.value(TokenModel.fromJson(json.decode(jsonString)));
  }

  @override
  Future<TokenModel> setAuthToken(TokenModel model) async {
    final result = await sharedPreferences.setString(
        CACHED_TOKEN, json.encode(model.toJson()));

    if (result == true) {
      return Future.value(
        TokenModel.fromJson(
          json.decode(
            sharedPreferences.getString(CACHED_TOKEN),
          ),
        ),
      );
    }
    throw CacheException();
  }

  @override
  bool verificarCredenciales(String authority) {
    if (authority.toLowerCase().contains('farmacia') ||
        authority.toLowerCase().contains('laboratorio'))
      return true;
    else
      throw AuthException();
  }
}
