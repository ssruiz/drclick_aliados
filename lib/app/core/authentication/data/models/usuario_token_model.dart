import 'package:meta/meta.dart';

import '../../domain/entities/token_entity.dart';

class TokenModel extends TokenEntity {
  TokenModel({
    @required String username,
    @required String email,
    @required String token,
    @required String authority,
  }) : super(
          username: username,
          email: email,
          token: token,
          authority: authority,
        );

  factory TokenModel.fromJson(Map<String, dynamic> json) => TokenModel(
        username: json["username"],
        email: json["email"],
        token: json["token"],
        authority: json["authority"],
      );

  Map<String, dynamic> toJson() => {
        "username": username,
        "email": email,
        "token": token,
        'authority': authority,
      };
}
