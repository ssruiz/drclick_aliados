import 'package:dartz/dartz.dart';

import '../errors/failures.dart';

abstract class UseCaseSync<Type> {
  Either<Failure, Type> call();
}
