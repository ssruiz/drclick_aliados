import 'package:dartz/dartz.dart';

import '../errors/failures.dart';

abstract class UseCaseWithParamsSync<Type, Params> {
  Either<Failure, Type> call(Params params);
}
