import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:logger/logger.dart';

import '../../../config/utils/constants.dart';

class DioService {
  static DioService _singletonHttp;
  Dio _http;

  factory DioService() {
    if (_singletonHttp == null) _singletonHttp = DioService._internal();
    return _singletonHttp;
  }
  DioService._internal() {
    _http = Dio();
    _http.options.baseUrl = AppConstants.API_URL;

    // interceptors
    _http.interceptors.add(
      InterceptorsWrapper(
        onRequest: (RequestOptions options) async {
          final path = options.path;
          final log = Logger();
          if (path.contains('oauth/token')) {
            final encode = base64.encode(
              utf8.encode(AppConstants.CLIENT_SECRET),
            );
            options.headers = {
              'Authorization': 'Basic $encode',
              'Content-type': 'application/x-www-form-urlencoded'
            };
            return options;
          } else
            log.d('No soy AUTH . Soy $path');
        },
        onError: (DioError e) async {
          return Response(statusCode: e.response.statusCode);
        },
      ),
    );
  }
  Dio get client => _http;

  dispose() {
    _http.close();
  }
}
