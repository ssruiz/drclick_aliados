class AppRoutes {
  static const SplashRoute = 'splash';
  static const LoginRoute = 'login';
  static const HomeRouteFarmacia = 'homeFarmacia';
  static const HomeRouteLaboratorio = 'homeLab';
  static const PacientesRoute = 'pacientes';
  static const RecetasRoute = 'recetas';
  static const PdfRoute = 'pdfview';
}
