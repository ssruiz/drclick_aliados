import 'package:drclick_aliados/app/modules/pdfView/ui/pdfView_binding.dart';
import 'package:drclick_aliados/app/modules/pdfView/ui/pdfView_page.dart';
import 'package:get/route_manager.dart';

import '../../modules/home_farmacia/ui/homeFarmacia_binding.dart';
import '../../modules/home_farmacia/ui/homeFarmacia_page.dart';
import '../../modules/home_laboratorio/ui/homeLaboratorio_binding.dart';
import '../../modules/home_laboratorio/ui/homeLaboratorio_page.dart';
import '../../modules/login/ui/login_binding.dart';
import '../../modules/login/ui/login_page.dart';
import '../../modules/pacientes/ui/pacientes_binding.dart';
import '../../modules/pacientes/ui/pacientes_page.dart';
import '../../modules/recetas/ui/recetas_binding.dart';
import '../../modules/recetas/ui/recetas_page.dart';
import '../../modules/splash/ui/splash_binding.dart';
import '../../modules/splash/ui/splash_page.dart';
import 'routes.dart';

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
      name: AppRoutes.SplashRoute,
      page: () => SplashPage(),
      binding: SplashBinding(),
      transition: Transition.zoom,
    ),
    GetPage(
      name: AppRoutes.LoginRoute,
      page: () => LoginPage(),
      binding: LoginBinding(),
      transition: Transition.zoom,
    ),
    GetPage(
      name: AppRoutes.HomeRouteFarmacia,
      page: () => HomeFarmaciaPage(),
      binding: HomeFarmaciaBinding(),
      transition: Transition.zoom,
    ),
    GetPage(
      name: AppRoutes.HomeRouteLaboratorio,
      page: () => HomeLaboratorioPage(),
      binding: HomeLaboratorioBinding(),
      transition: Transition.zoom,
    ),
    GetPage(
      name: AppRoutes.PacientesRoute,
      page: () => PacientesPage(),
      binding: PacientesBinding(),
      transition: Transition.zoom,
    ),
    GetPage(
      name: AppRoutes.RecetasRoute,
      page: () => RecetasPage(),
      binding: RecetasBinding(),
      transition: Transition.zoom,
    ),
    GetPage(
      name: '${AppRoutes.PdfRoute}',
      page: () => PdfViewPage(),
      binding: PdfViewBinding(),
      transition: Transition.zoom,
    ),
  ];
}
