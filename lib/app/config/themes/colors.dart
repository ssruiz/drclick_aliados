import 'package:flutter/material.dart';

class AppColors {
  static const primaryColor = Color(0xff2B4A7A);
  static const secondaryColor = Color(0xff77C3E9);
}
