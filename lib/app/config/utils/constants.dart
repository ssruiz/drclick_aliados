class AppConstants {
  static const API_URL = 'https://dev.drclick.com.py/DrClickServer/';
  static const CLIENT_SECRET = 'angularClientIdPassword:secret';
  static const AUTH_FARMACIA = 'farmacia';
  static const AUTH_LABS = 'laboratorios';
}
