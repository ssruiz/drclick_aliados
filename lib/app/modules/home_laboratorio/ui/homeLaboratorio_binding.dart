import 'package:get/get.dart';

import 'homeLaboratorio_controller.dart';

class HomeLaboratorioBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomeLaboratorioController());
  }
}
