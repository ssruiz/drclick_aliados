// To parse this JSON data, do
//
//     final pacienteModel = pacienteModelFromJson(jsonString);

import 'dart:convert';

import 'package:drclick_aliados/app/modules/pacientes/data/models/tipo_documento_model.dart';
import 'package:drclick_aliados/app/modules/pacientes/domain/entities/persona_entity.dart';
import 'package:meta/meta.dart';

PacienteModel pacienteModelFromJson(String str) =>
    PacienteModel.fromJson(json.decode(str));

String pacienteModelToJson(PacienteModel data) => json.encode(data.toJson());

class PacienteModel extends PacienteEntity {
  PacienteModel({
    @required idPersona,
    @required tipoDocumento,
    @required nrodoc,
    @required nombreapellido,
    @required correo,
    @required celular,
    @required direccion,
    @required sexo,
    @required fechaNacimiento,
    @required profesion,
    @required ocupacion,
    @required ciudadResidencia,
    @required ciudadNacimiento,
  }) : super(
          celular: celular,
          ciudadNacimiento: ciudadNacimiento,
          ciudadResidencia: ciudadResidencia,
          correo: correo,
          direccion: direccion,
          fechaNacimiento: fechaNacimiento,
          idPersona: idPersona,
          nombreapellido: nombreapellido,
          nrodoc: nrodoc,
          ocupacion: ocupacion,
          profesion: profesion,
          sexo: sexo,
          tipoDocumento: tipoDocumento,
        );

  factory PacienteModel.fromJson(Map<String, dynamic> json) => PacienteModel(
        idPersona: json["idPersona"],
        tipoDocumento: TipoDocumentoModel.fromJson(json["tipoDocumento"]),
        nrodoc: json["nrodoc"],
        nombreapellido: json["nombreapellido"],
        correo: json["correo"],
        celular: json["celular"],
        direccion: json["direccion"],
        sexo: json["sexo"],
        fechaNacimiento: json["fechaNacimiento"],
        profesion: json["profesion"],
        ocupacion: json["ocupacion"],
        ciudadResidencia: json["ciudadResidencia"],
        ciudadNacimiento: json["ciudadNacimiento"],
      );

  Map<String, dynamic> toJson() => {
        "idPersona": idPersona,
        "tipoDocumento": TipoDocumentoModel.toJson(tipoDocumento),
        "nrodoc": nrodoc,
        "nombreapellido": nombreapellido,
        "correo": correo,
        "celular": celular,
        "direccion": direccion,
        "sexo": sexo,
        "fechaNacimiento": fechaNacimiento,
        "profesion": profesion,
        "ocupacion": ocupacion,
        "ciudadResidencia": ciudadResidencia,
        "ciudadNacimiento": ciudadNacimiento,
      };
}
