import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/errors/failures.dart';
import '../../domain/entities/persona_entity.dart';
import '../../domain/repository/pacientes_repository.dart';
import '../datasources/remote_data_source.dart';

class PacientesRepositoryImpl implements PacientesRepository {
  final PacienteRemoteDataSource remoteDataSource;

  PacientesRepositoryImpl({@required this.remoteDataSource});
  @override
  Future<Either<Failure, PacienteEntity>> buscarPorCi(String ci) async {
    try {
      return Right(await remoteDataSource.buscarPaciente(ci));
    } catch (e) {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> verificarPaciente(
      PacienteEntity paciente) async {
    try {
      return Right(await remoteDataSource.verificarPaciente(paciente));
    } catch (e) {
      return Left(ServerFailure());
    }
  }
}
