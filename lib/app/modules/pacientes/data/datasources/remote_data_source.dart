import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../../../config/utils/constants.dart';
import '../../../../core/shared/dio/dio_config.dart';
import '../../../../core/shared/errors/exceptions.dart';
import '../models/paciente_model.dart';

abstract class PacienteRemoteDataSource {
  /// Realiza la peticion de get y busca un paciente por CI. Devuelve un [PacienteModel] si se encuentra.
  ///
  /// Throws [ServerException] en cualquier otro caso
  Future<PacienteModel> buscarPaciente(String ci);

  /// Realiza la verificaion del paciente. Retorna [true] si se encuentra registrado [false] en casos contrario.
  ///
  /// Throws [ServerException] en cualquier otro caso
  Future<bool> verificarPaciente(PacienteModel paciente);
}

class PacienteRemoteDataSourceImpl implements PacienteRemoteDataSource {
  final DioService dataSource;

  PacienteRemoteDataSourceImpl({@required this.dataSource});

  @override
  Future<PacienteModel> buscarPaciente(String ci) async {
    final url = AppConstants.API_URL + 'public/persona/$ci';

    final Response res = await dataSource.client.get(url);
    if (res.statusCode == 200) {
      final Map<String, dynamic> data = res.data;

      if (data.isEmpty) throw ServerException();

      final token = PacienteModel.fromJson(data);
      return token;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<bool> verificarPaciente(PacienteModel paciente) {
    return Future.value(true);
  }
}
