import 'package:get/get.dart';
import 'package:reactive_forms/reactive_forms.dart';

import '../../../core/utils/logger/logger_service.dart';
import '../../../core/utils/notifications/notificacion_service.dart';
import '../../../core/utils/notifications/notifications_keys.dart';
import '../domain/entities/persona_entity.dart';
import '../domain/usecases/buscar_paciente_ci.dart';
import '../domain/usecases/verificar_paciente.dart';

class PacientesController extends GetxController {
  RxBool _buscando = false.obs;
  RxBool _registrado = false.obs;
  RxBool _existe = false.obs;
  bool get buscando => _buscando.value;

  Rx<PacienteEntity> _paciente = PacienteEntity(
    idPersona: null,
    tipoDocumento: null,
    nrodoc: null,
    nombreapellido: null,
    correo: null,
    celular: null,
    direccion: null,
    sexo: null,
    fechaNacimiento: null,
    profesion: null,
    ocupacion: null,
    ciudadResidencia: null,
    ciudadNacimiento: null,
  ).obs;

  PacienteEntity get paciente => _paciente.value;
  bool get registrado => _registrado.value;
  bool get existe => _existe.value;

  final noti = Get.find<NotificationService>();
  final FormGroup filtroBusquedaForm = FormGroup({
    'busquedaFiltro': FormControl(
      value: '',
      validators: [Validators.number, Validators.minLength(7)],
    ),
  });

  void buscarPacienteCi() async {
    final control = filtroBusquedaForm.control('busquedaFiltro');

    final ci = control.value;
    final log = Get.find<LoggerService>();

    if (filtroBusquedaForm.valid) {
      _buscando.value = true;
      _registrado.value = false;
      _existe.value = false;

      final BuscarPacienteCi buscarPacienteCi = Get.find<BuscarPacienteCi>();
      log.logger.d('Buscando CI => $ci');
      final res = await buscarPacienteCi(ci);
      res.fold(
        (l) {
          log.logger.e('Error');
          _buscando.value = false;
          noti.mostrarSnackBar(
            color: NotiKey.ERROR,
            mensaje: 'Paciente no encontrado. Verifique la CI',
            titulo: 'Búsqueda',
            position: SnackPosition.BOTTOM,
          );
        },
        (r) async {
          log.logger.d(r);
          _paciente.value = r;
          final VerificarPaciente verificarPaciente =
              Get.find<VerificarPaciente>();
          final resultado = await verificarPaciente(r);

          resultado.fold(
            (error) => null,
            (success) {
              _buscando.value = false;
              _existe.value = true;
              _registrado.value = success;
            },
          );
        },
      );
    }
  }
}
