import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

import '../../pacientes_controller.dart';
import 'form_card_paciente_w.dart';
import 'header_card_w.dart';

class CardPaciente extends GetView<PacientesController> {
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          HeaderCardPaciente(tipo: controller.registrado ? 1 : 0),
          Flexible(
            child: FormCardPaciente(),
          ),
        ],
      ),
    );
  }
}
