import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

import '../../../../../config/themes/colors.dart';
import '../../../../../config/themes/fonts.dart';
import '../../pacientes_controller.dart';
import 'botones_card_w.dart';

class FormCardPaciente extends GetView<PacientesController> {
  @override
  Widget build(BuildContext context) {
    final paciente = controller.paciente;
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListItemCardInfo(
              data: paciente.nombreapellido,
              titulo: 'Nombre',
              icon: FontAwesomeIcons.user,
            ),
            ListItemCardInfo(
              data: paciente.nrodoc,
              titulo: 'Nro. Documento',
              icon: FontAwesomeIcons.idBadge,
            ),
            ListItemCardInfo(
              data: paciente.correo,
              titulo: 'Email',
              icon: FontAwesomeIcons.envelope,
            ),
            ListItemCardInfo(
              data: paciente.fechaNacimiento,
              titulo: 'Fecha Nac.',
              icon: FontAwesomeIcons.birthdayCake,
            ),
            ListItemCardInfo(
              data: paciente.sexo,
              titulo: 'Sexo',
              icon: FontAwesomeIcons.venusMars,
            ),
            SizedBox(
              height: 10,
            ),
            if (!controller.registrado) RegistrarCancelarWidget(),
          ],
        ),
      ),
    );
  }
}

class ListItemCardInfo extends StatelessWidget {
  const ListItemCardInfo({
    @required this.data,
    @required this.titulo,
    @required this.icon,
  });

  final String data;
  final String titulo;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      dense: true,
      trailing: FaIcon(
        icon,
        color: AppColors.secondaryColor,
      ),
      title: Text(
        '$data',
        style: AppFonts.primaryFont.copyWith(fontSize: 14),
      ),
      subtitle: Text(
        '$titulo',
        style: AppFonts.primaryFont.copyWith(fontSize: 14),
      ),
    );
  }
}
