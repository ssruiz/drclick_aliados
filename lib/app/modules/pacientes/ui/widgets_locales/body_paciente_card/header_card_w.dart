import 'package:flutter/material.dart';

import '../../../../../config/themes/colors.dart';
import '../../../../../config/themes/fonts.dart';

class HeaderCardPaciente extends StatelessWidget {
  const HeaderCardPaciente({
    @required this.tipo,
  });

  final int tipo;
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: ConstrainedBox(
        constraints: BoxConstraints(maxHeight: 45, minHeight: 25),
        child: Container(
          width: double.infinity,
          color: AppColors.primaryColor,
          child: Center(
            child: Text(
              tipo == 1 ? 'Paciente ya registrado' : 'Paciente no registrado',
              style: AppFonts.primaryFont.copyWith(
                fontSize: 18,
                color: Colors.white,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
