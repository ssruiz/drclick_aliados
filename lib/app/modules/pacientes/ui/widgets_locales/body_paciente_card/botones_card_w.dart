import 'package:flutter/material.dart';

import '../../../../../config/themes/colors.dart';

class RegistrarCancelarWidget extends StatelessWidget {
  const RegistrarCancelarWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        ConstrainedBox(
          constraints: BoxConstraints(minWidth: 90, maxWidth: 150),
          child: OutlinedButton(
            child: Text('Cancelar'),
            onPressed: () => {},
          ),
        ),
        ConstrainedBox(
          constraints: BoxConstraints(minWidth: 90, maxWidth: 150),
          child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor:
                  MaterialStateProperty.all<Color>(AppColors.secondaryColor),
            ),
            child: Text('Registrar'),
            onPressed: () => {},
          ),
        ),
      ],
    );
  }
}
