import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

import '../../pacientes_controller.dart';
import 'card_paciente_w.dart';

class ContenidoPacienteWidget extends GetView<PacientesController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Obx(
        () {
          if (controller.existe) {
            return Container(
              child: CardPaciente(),
            );
          } else
            return Container();
        },
      ),
    );
  }
}
