import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';

import '../pacientes_controller.dart';

class BuscandoProgressWidget extends GetView<PacientesController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Obx(
        () {
          if (controller.buscando) return CircularProgressIndicator();
          return Container();
        },
      ),
    );
  }
}
