import 'package:flutter/material.dart';

import 'body_paciente_card/contenido_paciente_w.dart';
import 'buscando_progress_w.dart';
import 'busqueda_input_pacientes_w.dart';

class BodyPacientes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          BusquedaPacienteWidget(),
          SizedBox(
            height: 15,
          ),
          Divider(),
          SizedBox(
            height: 15,
          ),
          BuscandoProgressWidget(),
          Flexible(child: ContenidoPacienteWidget()),
        ],
      ),
    );
  }
}
