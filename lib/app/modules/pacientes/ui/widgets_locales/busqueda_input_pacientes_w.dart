import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:reactive_forms/reactive_forms.dart';

import '../../../../config/themes/colors.dart';
import '../../../../config/themes/fonts.dart';
import '../pacientes_controller.dart';

class BusquedaPacienteWidget extends GetView<PacientesController> {
  @override
  Widget build(BuildContext context) {
    return ReactiveForm(
      formGroup: controller.filtroBusquedaForm,
      child: Container(
        child: _InputWidget(
          controlName: 'busquedaFiltro',
          labelText: 'Buscar Paciente',
          hintText: 'Ingrese CI a buscar',
          icon: FontAwesomeIcons.search,
        ),
      ),
    );
  }
}

class _InputWidget extends GetView<PacientesController> {
  const _InputWidget({
    @required this.controlName,
    @required this.icon,
    @required this.hintText,
    this.suffixIcon1,
    this.obscureText,
    @required this.labelText,
  });

  final String controlName;
  final String hintText;
  final String labelText;
  final IconData icon;
  final bool obscureText;
  final IconData suffixIcon1;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ReactiveTextField(
        formControlName: controlName,
        keyboardType: TextInputType.text,
        validationMessages: (_) => {
          ValidationMessage.number: 'Debe ingresar un valor de ci válido',
          ValidationMessage.minLength: 'Debe ingresar un valor de ci válido',
        },
        decoration: InputDecoration(
          isDense: true,
          suffixIcon: IconButton(
            icon: FaIcon(
              icon,
              color: AppColors.primaryColor,
              size: 15,
            ),
            onPressed: controller.buscarPacienteCi,
          ),
          hintText: hintText,
          labelText: labelText,
          labelStyle: AppFonts.primaryFont.copyWith(
            fontSize: 15,
          ),
          hintStyle: AppFonts.primaryFont.copyWith(
            fontSize: 18,
          ),
        ),
        style: AppFonts.primaryFont.copyWith(fontSize: 18),
      ),
    );
  }
}
