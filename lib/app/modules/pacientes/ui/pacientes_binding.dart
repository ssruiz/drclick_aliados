import 'package:get/get.dart';

import 'pacientes_controller.dart';

class PacientesBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PacientesController());
  }
}
