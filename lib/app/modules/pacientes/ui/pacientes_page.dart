import '../../shared/global_widgets/app_bar_secciones_w.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../config/themes/fonts.dart';
import 'pacientes_controller.dart';
import 'widgets_locales/body_pacientes_page_w.dart';

class PacientesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PacientesController>(
      builder: (_) {
        return SafeArea(
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBarSeccion(
              titulo: 'Pacientes',
            ),
            body: BodyPacientes(),
          ),
        );
      },
    );
  }
}
