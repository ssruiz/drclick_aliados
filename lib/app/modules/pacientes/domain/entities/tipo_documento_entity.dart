import 'package:meta/meta.dart';

class TipoDocumentoEntity {
  TipoDocumentoEntity({
    @required this.idTipoDocumento,
    @required this.descripcion,
    @required this.estado,
    @required this.abreviatura,
  });

  final int idTipoDocumento;
  final String descripcion;
  final bool estado;
  final String abreviatura;
}
