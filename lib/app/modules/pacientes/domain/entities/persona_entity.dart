// To parse this JSON data, do
//
//     final persona = personaFromJson(jsonString);

import 'package:drclick_aliados/app/modules/pacientes/domain/entities/tipo_documento_entity.dart';
import 'package:meta/meta.dart';

class PacienteEntity {
  PacienteEntity({
    @required this.idPersona,
    @required this.tipoDocumento,
    @required this.nrodoc,
    @required this.nombreapellido,
    @required this.correo,
    @required this.celular,
    @required this.direccion,
    @required this.sexo,
    @required this.fechaNacimiento,
    @required this.profesion,
    @required this.ocupacion,
    @required this.ciudadResidencia,
    @required this.ciudadNacimiento,
  });

  final int idPersona;
  final TipoDocumentoEntity tipoDocumento;
  final String nrodoc;
  final String nombreapellido;
  final String correo;
  final String celular;
  final String direccion;
  final String sexo;
  final String fechaNacimiento;
  final String profesion;
  final String ocupacion;
  final String ciudadResidencia;
  final String ciudadNacimiento;

  @override
  String toString() {
    return 'PacienteEntity(idPersona: $idPersona, tipoDocumento: $tipoDocumento, nrodoc: $nrodoc, nombreapellido: $nombreapellido, correo: $correo, celular: $celular, direccion: $direccion, sexo: $sexo, fechaNacimiento: $fechaNacimiento, profesion: $profesion, ocupacion: $ocupacion, ciudadResidencia: $ciudadResidencia, ciudadNacimiento: $ciudadNacimiento)';
  }
}
