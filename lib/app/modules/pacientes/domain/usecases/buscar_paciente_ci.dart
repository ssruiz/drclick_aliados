import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/errors/failures.dart';
import '../../../../core/shared/usecases/usecase_params.dart';
import '../entities/persona_entity.dart';
import '../repository/pacientes_repository.dart';

class BuscarPacienteCi implements UseCaseWithParams<PacienteEntity, String> {
  final PacientesRepository repository;

  BuscarPacienteCi({@required this.repository});

  @override
  Future<Either<Failure, PacienteEntity>> call(String params) =>
      repository.buscarPorCi(params);
}
