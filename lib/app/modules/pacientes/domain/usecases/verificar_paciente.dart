import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/errors/failures.dart';
import '../../../../core/shared/usecases/usecase_params.dart';
import '../entities/persona_entity.dart';
import '../repository/pacientes_repository.dart';

class VerificarPaciente implements UseCaseWithParams<bool, PacienteEntity> {
  final PacientesRepository repository;

  VerificarPaciente({@required this.repository});

  @override
  Future<Either<Failure, bool>> call(PacienteEntity params) =>
      repository.verificarPaciente(params);
}
