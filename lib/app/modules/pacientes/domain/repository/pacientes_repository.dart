import 'package:dartz/dartz.dart';

import '../../../../core/shared/errors/failures.dart';
import '../entities/persona_entity.dart';

abstract class PacientesRepository {
  Future<Either<Failure, PacienteEntity>> buscarPorCi(String ci);
  Future<Either<Failure, bool>> verificarPaciente(PacienteEntity ci);
}
