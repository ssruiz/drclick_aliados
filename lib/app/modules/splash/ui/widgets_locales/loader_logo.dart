import 'package:flutter/material.dart';

import '../../../../config/themes/colors.dart';
import '../../../../core/utils/extensions/animations_extension.dart';

class LoaderLogo extends StatelessWidget {
  const LoaderLogo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: [
        Container(
          decoration: BoxDecoration(
            color: AppColors.secondaryColor,
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              flex: 2,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      height: 250,
                      image: AssetImage(
                        'assets/logo3.png',
                      ),
                    ).animateBouncing(),
                    SizedBox(
                      height: 50,
                    ),
                    CircularProgressIndicator(
                      backgroundColor: Colors.white,
                      strokeWidth: 5,
                    )
                  ],
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}
