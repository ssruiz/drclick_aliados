import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'splash_controller.dart';
import 'widgets_locales/loader_logo.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
      builder: (_) {
        return SafeArea(
          child: Scaffold(
            body: LoaderLogo(),
          ),
        );
      },
    );
  }
}
