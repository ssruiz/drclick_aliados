import 'package:get/get.dart';

import '../../../config/routes/navigator.dart';
import '../../../config/routes/routes.dart';
import '../../../config/utils/constants.dart';
import '../../../core/authentication/domain/usecases/get_auth_token.dart';

class SplashController extends GetxController {
  @override
  onReady() {
    super.onReady();
    verifySession();
  }

  void verifySession() async {
    // final deleteTOken = Get.find<DeleteAuthToken>();
    // await deleteTOken();
    final getToken = Get.find<GetAuthToken>();
    final nav = Get.find<NavigatorController>();
    final respuesta = await getToken();

    respuesta.fold(
      (l) => Future.delayed(
        Duration(seconds: 2),
        () => nav.goToOff(AppRoutes.LoginRoute),
      ),
      (r) => Future.delayed(
        Duration(seconds: 2),
        () => r.authority.toLowerCase().contains(AppConstants.AUTH_FARMACIA)
            ? nav.goToAndClean(AppRoutes.HomeRouteFarmacia)
            : nav.goToAndClean(AppRoutes.HomeRouteLaboratorio),
      ),
    );
  }
}
