import 'package:get/get.dart';
import 'package:reactive_forms/reactive_forms.dart';

import '../../../config/routes/navigator.dart';
import '../../../config/routes/routes.dart';
import '../../../config/utils/constants.dart';
import '../../../core/authentication/domain/usecases/set_auth_token.dart';
import '../../../core/authentication/domain/usecases/verificar_credenciales.dart';
import '../../../core/utils/notifications/notificacion_service.dart';
import '../../../core/utils/notifications/notifications_keys.dart';
import '../domain/usecases/login_usecase.dart';

class LoginController extends GetxController {
  final nav = Get.find<NavigatorController>();
  final noti = Get.find<NotificationService>();

  RxBool _ignore = false.obs;
  RxBool _ignore2 = false.obs;
  RxBool _obscure = true.obs;

  RxBool get ignore => _ignore;
  RxBool get ignore2 => _ignore2;
  bool get mostrarPassword => _obscure.value;

  void cambiarMostrarPassword() => _obscure.value = !_obscure.value;

  final FormGroup loginForm = FormGroup({
    'user': FormControl(
      value: '',
      validators: [Validators.required],
    ),
    'password': FormControl(
      value: '',
      validators: [
        Validators.required,
      ],
    ),
  });

  final FormGroup recuperarPasswordForm = FormGroup({
    'username': FormControl(
      value: '',
      validators: [Validators.required],
    ),
  });

  String get username => this.loginForm.control('user').value;
  String get password => this.loginForm.control('password').value;

  void login() async {
    final LoginSistema loginSistema = Get.find<LoginSistema>();

    _ignore.value = true;
    final result = await loginSistema(
      ParamsLogin(
        username: username,
        password: password,
      ),
    );
    _ignore.value = false;

    result.fold(
      (l) {
        noti.mostrarSnackBar(
          color: NotiKey.ERROR,
          titulo: 'Crendenciales inválidas',
          mensaje: "Usuario o contraseña incorrectos",
        );
        _obscure.value = true;
        loginForm.control('user').value = '';
        loginForm.control('password').value = '';
        loginForm.focus('user');
      },
      (r) async {
        final VerificarCredenciales verificarCredenciales =
            Get.find<VerificarCredenciales>();

        final verificar = verificarCredenciales(r.usuario.authority);

        verificar.fold(
          (l) => noti.mostrarSnackBar(
            color: NotiKey.ERROR,
            titulo: "Usuario sin autorización",
            mensaje: 'Crendenciales inválidas',
          ),
          (_) async {
            final SetAuthToken setAuthToken = Get.find<SetAuthToken>();
            await setAuthToken(
              SetTokenParams(
                  email: r.usuario.persona.correo,
                  username: r.usuario.username,
                  token: r.accessToken,
                  authority: r.usuario.authority),
            );

            if (r.usuario.authority
                .toLowerCase()
                .contains(AppConstants.AUTH_FARMACIA))
              nav.goToAndClean(AppRoutes.HomeRouteFarmacia);
            else
              nav.goToAndClean(AppRoutes.HomeRouteFarmacia);
          },
        );
      },
    );
  }

  void recuperarPassword() async {
    _ignore2.value = true;
    Future.delayed(
      Duration(milliseconds: 900),
      () => _ignore2.value = false,
    );
    print('Recuperando');
  }

  void cerrarDialogoRecuperar() async {
    recuperarPasswordForm.resetState(
      {
        'username': ControlState(
          value: '',
        ),
      },
    );
    Get.back();
  }
}
