import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:reactive_forms/reactive_forms.dart';

import '../../../../config/themes/colors.dart';
import '../../../../config/themes/fonts.dart';
import '../../../../core/utils/extensions/animations_extension.dart';
import '../../../shared/global_widgets/send_form_button_w.dart';
import '../login_controller.dart';
import 'recuperar_password_w.dart';

class LoginView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: 15),
        Align(
          alignment: Alignment.topCenter,
          child: AspectRatio(
            aspectRatio: 3 / 1.5,
            child: Image(
              image: AssetImage('assets/logo3.png'),
              height: 180,
            ).animateScale(),
          ),
        ),
        Text(
          'Alianzas',
          style: AppFonts.primaryFont.copyWith(
            fontSize: 25,
            letterSpacing: 3,
            fontWeight: FontWeight.w600,
            color: AppColors.primaryColor,
          ),
        ).animateScale(delay: 1450),
        Expanded(
          child: LoginForm(),
        )
      ],
    );
  }
}

class LoginForm extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    final form = controller.loginForm;
    return SingleChildScrollView(
      child: ReactiveForm(
        formGroup: form,
        child: Container(
          width: Get.width * 0.85,
          padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 20),
          margin: EdgeInsets.symmetric(vertical: 20.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 3.0,
                offset: Offset(0, 1.0),
                spreadRadius: 1.5,
              ),
            ],
          ),
          child: Column(
            children: [
              Text(
                'Ingreso',
                style: AppFonts.primaryFont.copyWith(
                  fontSize: 25,
                  letterSpacing: 3.0,
                  fontWeight: FontWeight.w500,
                ),
              ),
              Divider(
                height: 35,
              ),
              SizedBox(
                height: 20.0,
              ),
              Container(
                child: _InputWidget(
                  controlName: 'user',
                  hintText: 'Usuario',
                  icon: FontAwesomeIcons.userAlt,
                  validationMessageRequired: 'Este campo es obligatorio',
                  suffixIcon: false,
                  obscureText: false,
                ),
              ),
              SizedBox(
                height: 25.0,
              ),
              Container(
                child: Obx(
                  () => _InputWidget(
                    controlName: 'password',
                    hintText: 'Contraseña',
                    obscureText: true,
                    icon: FontAwesomeIcons.lock,
                    validationMessageRequired: 'Este campo es obligatorio',
                    suffixIcon: true,
                    suffixIcon1: controller.mostrarPassword
                        ? FontAwesomeIcons.eye
                        : FontAwesomeIcons.eyeSlash,
                    controller: controller,
                  ),
                ),
              ),
              SizedBox(
                height: 35,
              ),
              ReactiveFormConsumer(
                builder: (context, form, child) {
                  return Obx(
                    () => IgnorePointer(
                      ignoring: controller.ignore.value,
                      child: FormularioSendButton(
                        controller: controller,
                        form: form,
                        funcion: controller.login,
                        width: double.infinity,
                        height: 50,
                        ignore: controller.ignore,
                      ),
                    ),
                  );
                },
              ),
              RecuperarPasswordTextWidget()
            ],
          ),
        ),
      ),
    );
  }
}

class _InputWidget extends StatelessWidget {
  const _InputWidget({
    @required this.controlName,
    @required this.validationMessageRequired,
    @required this.icon,
    @required this.hintText,
    @required this.suffixIcon,
    this.suffixIcon1,
    this.controller,
    this.obscureText,
  });

  final String controlName;
  final String validationMessageRequired;
  final String hintText;
  final IconData icon;
  final bool suffixIcon;
  final bool obscureText;
  final IconData suffixIcon1;
  final LoginController controller;

  @override
  Widget build(BuildContext context) {
    return ReactiveTextField(
      formControlName: controlName,
      obscureText: obscureText ? controller.mostrarPassword : false,
      validationMessages: (_) => {
        ValidationMessage.required: validationMessageRequired,
      },
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: FaIcon(
          icon,
          color: AppColors.secondaryColor,
          size: 20,
        ),
        hintText: hintText,
        suffixIcon: suffixIcon
            ? IconButton(
                icon: FaIcon(suffixIcon1),
                onPressed: controller.cambiarMostrarPassword,
              )
            : null,
      ),
      style: AppFonts.primaryFont.copyWith(fontSize: 18),
    );
  }
}
