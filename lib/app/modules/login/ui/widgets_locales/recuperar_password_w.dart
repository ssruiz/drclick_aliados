import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:reactive_forms/reactive_forms.dart';

import '../../../../config/themes/colors.dart';
import '../../../../config/themes/fonts.dart';
import '../../../shared/global_widgets/send_form_button_w.dart';
import '../login_controller.dart';

class RecuperarPasswordTextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Padding(
        padding: EdgeInsets.all(5),
        child: Text(
          '¿Olvidó su contraseña?',
          style: AppFonts.primaryFont.copyWith(
            fontSize: 15,
            color: AppColors.secondaryColor,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      onPressed: abrirDialogo,
    );
  }

  abrirDialogo() {
    Get.dialog(
      _DialogRecuperarPassword(),
      useSafeArea: true,
      barrierDismissible: false,
    );
  }
}

class _DialogRecuperarPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Dialog(
        clipBehavior: Clip.hardEdge,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        child: _StackDialog(),
      ),
    );
  }
}

class _StackDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            left: 20,
            top: 45.0 + 20.0,
            right: 20,
            bottom: 20.0,
          ),
          margin: EdgeInsets.only(top: 45.0),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black,
                offset: Offset(0, 10),
                blurRadius: 10,
              ),
            ],
          ),
          child: _Formulario(),
        ),
        Positioned(
          left: 20,
          right: 20,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 45,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(45)),
              child: Image.asset(
                "assets/logo3.png",
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class _Formulario extends StatelessWidget {
  final LoginController loginController = Get.find<LoginController>();
  @override
  Widget build(BuildContext context) {
    return ReactiveForm(
      formGroup: loginController.recuperarPasswordForm,
      child: ConstrainedBox(
        constraints: BoxConstraints(minHeight: 150, maxHeight: 200),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Recuperar Contraseña',
              style: AppFonts.primaryFont
                  .copyWith(fontSize: 22, fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 5,
            ),
            Divider(
              height: 1,
            ),
            SizedBox(
              height: 20,
            ),
            _InputWidget(
              controlName: 'username',
              validationMessageRequired: 'Campo obligatorio',
              icon: FontAwesomeIcons.userAlt,
              hintText: 'Ingrese su usuario',
              suffixIcon: false,
              controller: loginController,
              obscureText: false,
              suffixIcon1: FontAwesomeIcons.accessibleIcon,
            ),
            Spacer(),
            Flexible(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  FlatButton(
                    onPressed: () => loginController.cerrarDialogoRecuperar(),
                    child: Text(
                      'Cancelar',
                      style: AppFonts.primaryFont.copyWith(
                        color: Colors.black,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  ReactiveFormConsumer(builder: (context, form, child) {
                    return Obx(
                      () => IgnorePointer(
                        ignoring: loginController.ignore2.value,
                        child: FormularioSendButton(
                          controller: loginController,
                          form: loginController.recuperarPasswordForm,
                          funcion: loginController.recuperarPassword,
                          width: 100,
                          height: 35,
                          ignore: loginController.ignore2,
                        ),
                        //     MaterialButton(
                        //   color: AppColors.primaryColor,
                        //   disabledColor: Colors.grey,
                        //   child: loginController.ignore2
                        //       ? Container(
                        //           child: CircularProgressIndicator(
                        //             backgroundColor: Colors.white,
                        //           ),
                        //         )
                        //       : Text(
                        //           'Recuperar',
                        //           style: TextStyle(
                        //             fontSize: 14,
                        //             color: Colors.white,
                        //           ),
                        //         ),
                        //   onPressed: loginController.recuperarPasswordForm.valid
                        //       ? loginController.recuperarPassword
                        //       : null,
                        // ),
                      ),
                    );
                  }),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _InputWidget extends StatelessWidget {
  const _InputWidget({
    @required this.controlName,
    @required this.validationMessageRequired,
    @required this.icon,
    @required this.hintText,
    @required this.suffixIcon,
    this.suffixIcon1,
    this.controller,
    this.obscureText,
  });

  final String controlName;
  final String validationMessageRequired;
  final String hintText;
  final IconData icon;
  final bool suffixIcon;
  final bool obscureText;
  final IconData suffixIcon1;
  final LoginController controller;

  @override
  Widget build(BuildContext context) {
    return ReactiveTextField(
      formControlName: controlName,
      obscureText: obscureText ? controller.mostrarPassword : false,
      validationMessages: (_) => {
        ValidationMessage.required: validationMessageRequired,
      },
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: FaIcon(
          icon,
          color: AppColors.primaryColor,
          size: 20,
        ),
        hintText: hintText,
        suffixIcon: suffixIcon
            ? IconButton(
                icon: FaIcon(suffixIcon1),
                onPressed: controller.cambiarMostrarPassword,
              )
            : null,
      ),
      style: AppFonts.primaryFont.copyWith(fontSize: 18),
    );
  }
}
