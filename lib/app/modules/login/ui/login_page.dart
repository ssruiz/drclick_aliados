import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'login_controller.dart';
import 'widgets_locales/bg_login.dart';
import 'widgets_locales/login_view.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(
      builder: (_) {
        return SafeArea(
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            body: Stack(
              fit: StackFit.expand,
              children: [
                BackgroundLogin2(),
                BackgroundLogin(),
                LoginView(),
              ],
            ),
          ),
        );
      },
    );
  }
}
