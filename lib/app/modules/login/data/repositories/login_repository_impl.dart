import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/errors/exceptions.dart';
import '../../../../core/shared/errors/failures.dart';
import '../../domain/entities/user_api_entity.dart';
import '../../domain/repository/login_repository.dart';
import '../datasources/remote_datasource.dart';

class LoginRepositoryImpl implements LoginRepository {
  LoginRemoteDataSource dataSource;

  LoginRepositoryImpl({@required this.dataSource});

  @override
  Future<Either<Failure, UserApiTokenEntity>> login(
      String username, String password) async {
    try {
      return Right(await dataSource.login(username, password));
    } on ServerException {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> restaurarPassword(String username) async {
    try {
      return Right(await dataSource.restaurarPassword(username));
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
