import 'package:meta/meta.dart';

import '../../domain/entities/persona_entity.dart';
import 'tipo_documento_model.dart';

class PersonaModel extends PersonaEntity {
  PersonaModel({
    @required idPersona,
    @required tipoDocumento,
    @required nrodoc,
    @required nombreapellido,
    @required correo,
    @required celular,
    @required sexo,
    @required fechaNacimiento,
    @required estadoCivil,
    @required profesion,
    @required escolaridad,
    @required ocupacion,
    @required ciudadResidencia,
    @required ciudadNacimiento,
    @required cantidadHnos,
    @required nroHijo,
  }) : super(
          cantidadHnos: cantidadHnos,
          celular: celular,
          ciudadNacimiento: ciudadNacimiento,
          ciudadResidencia: ciudadResidencia,
          correo: correo,
          escolaridad: escolaridad,
          estadoCivil: estadoCivil,
          fechaNacimiento: fechaNacimiento,
          idPersona: idPersona,
          nombreapellido: nombreapellido,
          nroHijo: nroHijo,
          nrodoc: nrodoc,
          ocupacion: ocupacion,
          profesion: profesion,
          sexo: sexo,
          tipoDocumento: tipoDocumento,
        );

  factory PersonaModel.fromJson(Map<String, dynamic> json) => PersonaModel(
        idPersona: json["idPersona"],
        tipoDocumento: TipoDocumentoModel.fromJson(json["tipoDocumento"]),
        nrodoc: json["nrodoc"],
        nombreapellido: json["nombreapellido"],
        correo: json["correo"],
        celular: json["celular"],
        sexo: json["sexo"],
        fechaNacimiento: json["fechaNacimiento"],
        estadoCivil: json["estadoCivil"],
        profesion: json["profesion"],
        escolaridad: json["escolaridad"],
        ocupacion: json["ocupacion"],
        ciudadResidencia: json["ciudadResidencia"],
        ciudadNacimiento: json["ciudadNacimiento"],
        cantidadHnos: json["cantidadHnos"],
        nroHijo: json["nroHijo"],
      );
}
