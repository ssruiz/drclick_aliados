import 'package:meta/meta.dart';

import '../../domain/entities/user_api_entity.dart';
import 'usuario_model.dart';

class UserApiTokenModel extends UserApiTokenEntity {
  UserApiTokenModel({
    @required String accessToken,
    @required String tokenType,
    @required String refreshToken,
    @required String scope,
    @required String clientId,
    @required UsuarioModel usuario,
  }) : super(
          accessToken: accessToken,
          tokenType: tokenType,
          refreshToken: refreshToken,
          clientId: clientId,
          scope: scope,
          usuario: usuario,
        );

  factory UserApiTokenModel.fromJson(Map<String, dynamic> json) =>
      UserApiTokenModel(
        accessToken: json["access_token"],
        tokenType: json["token_type"],
        refreshToken: json["refresh_token"],
        scope: json["scope"],
        clientId: json["clientId"],
        usuario: UsuarioModel.fromJson(json["usuario"]),
      );
}
