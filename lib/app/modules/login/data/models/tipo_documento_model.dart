import 'package:meta/meta.dart';

import '../../domain/entities/tipo_documento_entity.dart';

class TipoDocumentoModel extends TipoDocumentoEntity {
  TipoDocumentoModel({
    @required idTipoDocumento,
    @required descripcion,
    @required estado,
    @required abreviatura,
  }) : super(
          abreviatura: abreviatura,
          descripcion: descripcion,
          estado: estado,
          idTipoDocumento: idTipoDocumento,
        );

  factory TipoDocumentoModel.fromJson(Map<String, dynamic> json) =>
      TipoDocumentoModel(
        idTipoDocumento: json["idTipoDocumento"],
        descripcion: json["descripcion"],
        estado: json["estado"],
        abreviatura: json["abreviatura"],
      );
}
