import 'package:meta/meta.dart';

import '../../domain/entities/usuario_entity.dart';
import 'persona_model.dart';

class UsuarioModel extends UsuarioEntity {
  UsuarioModel(
      {@required String password,
      @required String username,
      @required bool enabled,
      @required int idUsuario,
      @required String img,
      @required bool google,
      @required uid,
      @required PersonaModel persona,
      @required String authority})
      : super(
          enabled: enabled,
          username: username,
          password: password,
          google: google,
          idUsuario: idUsuario,
          img: img,
          persona: persona,
          uid: uid,
          authority: authority,
        );

  factory UsuarioModel.fromJson(Map<String, dynamic> json) {
    return UsuarioModel(
      password: json["password"],
      username: json["username"],
      enabled: json["enabled"],
      idUsuario: json["idUsuario"],
      img: json["img"],
      google: json["google"],
      uid: json["uid"],
      persona: PersonaModel.fromJson(json["persona"]),
      authority: (json["authorities"][0] as Map)["authority"],
    );
  }
}
