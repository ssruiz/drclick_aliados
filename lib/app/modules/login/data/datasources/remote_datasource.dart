import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

import '../../../../config/utils/constants.dart';
import '../../../../core/shared/dio/dio_config.dart';
import '../../../../core/shared/errors/exceptions.dart';
import '../../domain/entities/user_api_entity.dart';
import '../models/user_api_model.dart';

abstract class LoginRemoteDataSource {
  /// Realiza la peticion de login y devuelve un [UserApiTokenEntity] si las credenciales son correctas
  ///
  /// Throws [ServerException] si las credenciales son incorrectas
  Future<UserApiTokenModel> login(String username, String password);

  Future<bool> restaurarPassword(String username);
}

class LoginRemoteDataSourceDio implements LoginRemoteDataSource {
  DioService dataSource;

  LoginRemoteDataSourceDio({@required this.dataSource});

  @override
  Future<UserApiTokenModel> login(String username, String password) async {
    final url = AppConstants.API_URL +
        'oauth/token?username=$username&password=$password&grant_type=password';

    final Response res = await dataSource.client.post(url);
    if (res.statusCode == 200) {
      final Map<String, dynamic> data = res.data;
      final token = UserApiTokenModel.fromJson(data);
      return token;
    } else {
      throw ServerException();
    }
  }

  @override
  Future<bool> restaurarPassword(String username) async {
    final url = AppConstants.API_URL;

    final Response res = await dataSource.client.post(url);
    if (res.statusCode == 200) {
      return true;
    } else {
      throw ServerException();
    }
  }
}
