import 'package:dartz/dartz.dart';

import '../../../../core/shared/errors/failures.dart';
import '../entities/user_api_entity.dart';

abstract class LoginRepository {
  Future<Either<Failure, UserApiTokenEntity>> login(
      String username, String password);

  Future<Either<Failure, bool>> restaurarPassword(String username);
}
