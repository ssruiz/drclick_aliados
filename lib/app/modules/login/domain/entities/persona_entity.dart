import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'tipo_documento_entity.dart';

class PersonaEntity extends Equatable {
  PersonaEntity({
    @required this.idPersona,
    @required this.tipoDocumento,
    @required this.nrodoc,
    @required this.nombreapellido,
    @required this.correo,
    @required this.celular,
    @required this.sexo,
    @required this.fechaNacimiento,
    @required this.estadoCivil,
    @required this.profesion,
    @required this.escolaridad,
    @required this.ocupacion,
    @required this.ciudadResidencia,
    @required this.ciudadNacimiento,
    @required this.cantidadHnos,
    @required this.nroHijo,
  });

  final int idPersona;
  final TipoDocumentoEntity tipoDocumento;
  final String nrodoc;
  final String nombreapellido;
  final String correo;
  final String celular;
  final String sexo;
  final String fechaNacimiento;
  final String estadoCivil;
  final String profesion;
  final String escolaridad;
  final String ocupacion;
  final String ciudadResidencia;
  final String ciudadNacimiento;
  final int cantidadHnos;
  final int nroHijo;

  @override
  List<Object> get props => [
        idPersona,
        tipoDocumento,
        nrodoc,
        nombreapellido,
        correo,
        celular,
        sexo,
        fechaNacimiento,
        estadoCivil,
        profesion,
        escolaridad,
        ocupacion,
        ciudadResidencia,
        ciudadNacimiento,
        cantidadHnos,
        nroHijo
      ];
}
