import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class TipoDocumentoEntity extends Equatable {
  TipoDocumentoEntity({
    @required this.idTipoDocumento,
    @required this.descripcion,
    @required this.estado,
    @required this.abreviatura,
  });

  final int idTipoDocumento;
  final String descripcion;
  final bool estado;
  final String abreviatura;

  @override
  List<Object> get props => [idTipoDocumento, descripcion, estado, abreviatura];
}
