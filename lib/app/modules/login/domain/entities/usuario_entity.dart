import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'persona_entity.dart';

class UsuarioEntity extends Equatable {
  UsuarioEntity({
    @required this.password,
    @required this.username,
    @required this.enabled,
    @required this.idUsuario,
    @required this.img,
    @required this.google,
    @required this.uid,
    @required this.persona,
    @required this.authority,
  });

  final String password;
  final String username;
  final bool enabled;
  final int idUsuario;
  final dynamic img;
  final bool google;
  final dynamic uid;
  final PersonaEntity persona;
  final String authority;

  @override
  List<Object> get props => [
        password,
        username,
        enabled,
        idUsuario,
        img,
        google,
        uid,
        persona,
        authority
      ];
}
