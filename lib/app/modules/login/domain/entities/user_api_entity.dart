import 'package:meta/meta.dart';

import 'usuario_entity.dart';

class UserApiTokenEntity {
  UserApiTokenEntity({
    @required this.accessToken,
    @required this.tokenType,
    @required this.refreshToken,
    @required this.scope,
    @required this.clientId,
    @required this.usuario,
  });

  final String accessToken;
  final String tokenType;
  final String refreshToken;
  final String scope;
  final String clientId;
  final UsuarioEntity usuario;
}
