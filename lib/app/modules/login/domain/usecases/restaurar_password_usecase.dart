import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/errors/failures.dart';
import '../../../../core/shared/usecases/usecase_params.dart';
import '../repository/login_repository.dart';

class RestaurarPassword implements UseCaseWithParams<bool, String> {
  final LoginRepository repository;

  RestaurarPassword({@required this.repository});

  @override
  Future<Either<Failure, bool>> call(String params) async =>
      repository.restaurarPassword(params);
}
