import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/errors/failures.dart';
import '../../../../core/shared/usecases/usecase_params.dart';
import '../entities/user_api_entity.dart';
import '../repository/login_repository.dart';

class LoginSistema
    implements UseCaseWithParams<UserApiTokenEntity, ParamsLogin> {
  final LoginRepository repository;

  LoginSistema({@required this.repository});

  @override
  Future<Either<Failure, UserApiTokenEntity>> call(ParamsLogin params) async =>
      repository.login(params.username, params.password);
}

class ParamsLogin {
  final String username;
  final String password;

  ParamsLogin({@required this.username, @required this.password});
}
