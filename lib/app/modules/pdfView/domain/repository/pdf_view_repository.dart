import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:drclick_aliados/app/core/shared/errors/failures.dart';

abstract class PdfViewRepository {
  Future<Either<Failure, bool>> checkFile(String path);
  //Future<Either<Failure, File>> getFromAsset(String path);
  Future<Either<Failure, String>> getFromUrl(String path);
}
