import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/errors/failures.dart';
import '../../../../core/shared/usecases/usecase_params.dart';
import '../repository/pdf_view_repository.dart';

class CheckFile implements UseCaseWithParams<bool, String> {
  final PdfViewRepository repository;
  CheckFile({@required this.repository});

  @override
  Future<Either<Failure, bool>> call(String path) => repository.checkFile(path);
}
