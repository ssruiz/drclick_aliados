import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/errors/failures.dart';
import '../../../../core/shared/usecases/usecase_params.dart';
import '../repository/pdf_view_repository.dart';

class GetPdfFromUrl implements UseCaseWithParams<String, String> {
  final PdfViewRepository repository;
  GetPdfFromUrl({@required this.repository});

  @override
  Future<Either<Failure, String>> call(String path) =>
      repository.getFromUrl(path);
}
