import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:drclick_aliados/app/config/utils/constants.dart';
import 'package:drclick_aliados/app/core/shared/dio/dio_config.dart';
import 'package:drclick_aliados/app/core/utils/logger/logger_service.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:path_provider/path_provider.dart';

abstract class PdfViewRemoteDataSource {
  /// Verifica si un archivo esta guardado localmente. Retorna [True] si se encuentra.
  ///
  /// Throws [ServerException] en cualquier otro caso
  Future<String> getFromUrl(String path);
}

class PdfViewRemoteDataSourceImpl implements PdfViewRemoteDataSource {
  final DioService dataSource;

  PdfViewRemoteDataSourceImpl({@required this.dataSource});

  @override
  Future<String> getFromUrl(String path) async {
    //var urlTest = 'https://www.ibm.com/downloads/cas/GJ5QVQ7X';
    var url = AppConstants.API_URL + 'private/document/$path';
    // var urlTest =
    var log = Get.find<LoggerService>().logger;
    //     'https://s25.q4cdn.com/967830246/files/doc_downloads/test.pdf';
    var data = await dataSource.client.get(
      url,
      options: Options(
        responseType: ResponseType.bytes,
      ),
    );

    var dir = await getTemporaryDirectory();
    File file = new File('${dir.path}/data.pdf');
    log.d(file.path);
    file.writeAsBytes(data.data);
    return file.path;
  }
}
