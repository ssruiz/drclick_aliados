import 'dart:io';

import 'package:flutter/services.dart';
import 'package:meta/meta.dart';
import 'package:path_provider/path_provider.dart';

abstract class PdfViewLocalDataSource {
  /// Verifica si un archivo esta guardado localmente. Retorna [True] si se encuentra.
  ///
  /// Throws [CachedException] en cualquier otro caso
  Future<bool> checkFile(String path);

  /// Retorna el archivo [File] si se encuentra.
  ///
  /// Throws [CachedException] en cualquier otro caso
  Future<File> getAssetFile(String path);
}

class PdfViewLocalDataSourceImpl implements PdfViewLocalDataSource {
  @override
  Future<bool> checkFile(String path) async => File(path).exists();

  @override
  Future<File> getAssetFile(String fileName) async {
    Directory appDocumentsDirectory =
        await getApplicationDocumentsDirectory(); // 1
    String appDocumentsPath = appDocumentsDirectory.path; // 2
    String filePath = '$appDocumentsPath/$fileName'; // 3

    File file = File(filePath);

    return file;
  }
}
