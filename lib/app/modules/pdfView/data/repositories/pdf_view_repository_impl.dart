import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/errors/failures.dart';
import '../../domain/repository/pdf_view_repository.dart';
import '../datasources/local_data_source.dart';
import '../datasources/remote_data_source.dart';

class PdfViewRepositoryImpl implements PdfViewRepository {
  final PdfViewLocalDataSource localDataSource;
  final PdfViewRemoteDataSource remoteDataSource;

  PdfViewRepositoryImpl(
      {@required this.localDataSource, @required this.remoteDataSource});

  @override
  Future<Either<Failure, bool>> checkFile(String path) async {
    try {
      return Right(await localDataSource.checkFile(path));
    } catch (e) {
      return Left(CacheFailure());
    }
  }

  @override
  Future<Either<Failure, String>> getFromUrl(String path) async {
    try {
      return Right(await remoteDataSource.getFromUrl(path));
    } catch (e) {
      return Left(CacheFailure());
    }
  }
}
