import 'package:get/get.dart';

import 'pdfView_controller.dart';

class PdfViewBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => PdfViewController());
  }
}
