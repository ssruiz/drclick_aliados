import 'dart:io';
import 'package:drclick_aliados/app/core/utils/logger/logger_service.dart';
import 'package:drclick_aliados/app/modules/pdfView/domain/usecases/get_from_url.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share/share.dart';

class PdfViewController extends GetxController {
  RxString pathFile = ''.obs;

  @override
  void onInit() {
    super.onInit();

    //pathFile.value = Get.arguments['path'];
    _init();
  }

  void _init() async {
    final GetPdfFromUrl getPdfFromUrl = Get.find<GetPdfFromUrl>();

    final result = await getPdfFromUrl(Get.arguments['path']);
    result.fold(
      (l) => print('error'),
      (r) {
        print(r);
        pathFile.value = r;
      },
    );
  }

  void compartir() async {
    await Share.shareFiles(['${pathFile.value}'], text: 'Compartir receta');
  }

  @override
  void onClose() async {
    super.onClose();
    Directory dir = await getTemporaryDirectory();
    dir.deleteSync(recursive: true);
  }
}
