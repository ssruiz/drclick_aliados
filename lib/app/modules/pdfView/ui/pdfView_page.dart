import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../shared/global_widgets/app_bar_secciones_w.dart';
import 'pdfView_controller.dart';

class PdfViewPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PdfViewController>(
      builder: (_) {
        return SafeArea(
          child: Scaffold(
            appBar: AppBarSeccion(
              titulo: 'Vista Receta',
            ),
            body: Obx(
              () => Container(
                child: _.pathFile.value == ''
                    ? Center(child: CircularProgressIndicator())
                    : PDFView(
                        filePath: _.pathFile.value,
                        onError: (error) {
                          print(error.toString());
                        },
                        onPageError: (page, error) {
                          print('$page: ${error.toString()}');
                        },
                        // onViewCreated: (PDFViewController pdfViewController) {
                        //   _controller.complete(pdfViewController);
                        // },
                        onPageChanged: (int page, int total) {
                          print('page change: $page/$total');
                        },
                      ),
              ),
            ),
            floatingActionButton: Obx(
              () => _.pathFile.value == ''
                  ? Container()
                  : FloatingActionButton(
                      onPressed: _.compartir,
                      child: FaIcon(
                        FontAwesomeIcons.shareAlt,
                        size: 19,
                        // color: AppColors.secondaryColor,
                      ),
                    ),
            ),
          ),
        );
      },
    );
  }
}
