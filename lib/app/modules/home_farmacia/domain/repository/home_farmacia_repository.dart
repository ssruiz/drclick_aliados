import 'package:dartz/dartz.dart';

import '../../../../core/shared/errors/failures.dart';

abstract class HomeFarmaciaRepository {
  Either<Failure, bool> logout();
}
