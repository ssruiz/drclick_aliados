import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/errors/failures.dart';
import '../../../../core/shared/usecases/usecase_sync.dart';
import '../repository/home_farmacia_repository.dart';

class CerrarSesion implements UseCaseSync<bool> {
  final HomeFarmaciaRepository repository;

  CerrarSesion({@required this.repository});

  @override
  Either<Failure, bool> call() => repository.logout();
}
