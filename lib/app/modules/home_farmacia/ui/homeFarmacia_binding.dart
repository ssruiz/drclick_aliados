import 'package:get/get.dart';

import 'homeFarmacia_controller.dart';

class HomeFarmaciaBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomeFarmaciaController());
  }
}
