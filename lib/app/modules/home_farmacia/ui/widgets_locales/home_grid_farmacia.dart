import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import '../../../../config/themes/fonts.dart';
import '../homeFarmacia_controller.dart';

class GridHomeFarmacia extends GetView<HomeFarmaciaController> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: controller.listadoUi.length,
      physics: BouncingScrollPhysics(),
      padding: EdgeInsets.all(20),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 5,
      ),
      itemBuilder: (BuildContext context, int index) {
        return _CardItem(
          icon: controller.listadoUi[index].icon,
          color: controller.listadoUi[index].color,
          titulo: controller.listadoUi[index].titulo,
        );
      },
    );
  }
}

class _CardItem extends GetView<HomeFarmaciaController> {
  const _CardItem({
    @required this.icon,
    @required this.titulo,
    @required this.color,
  });

  final IconData icon;
  final String titulo;
  final Color color;
  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25),
      ),
      color: color,
      elevation: 1.8,
      child: InkWell(
        onTap: () => controller.tapGridItem(titulo),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FaIcon(
              icon,
              size: 25,
              color: Colors.white,
            ),
            SizedBox(height: 10),
            Text(
              titulo,
              style: AppFonts.primaryFont.copyWith(
                color: Colors.white,
                fontSize: 15,
              ),
            )
          ],
        ),
      ),
    );
  }
}
