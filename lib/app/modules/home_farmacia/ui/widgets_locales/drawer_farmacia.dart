import '../../../../config/themes/fonts.dart';
import '../homeFarmacia_controller.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

import '../../../../config/themes/colors.dart';

class DrawerHomeFarmacia extends GetView<HomeFarmaciaController> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            padding: EdgeInsets.all(20),
            child: CircleAvatar(
              radius: 10,
              backgroundColor: AppColors.secondaryColor,
              child: Text('User'),
            ),
            decoration: BoxDecoration(
              color: AppColors.primaryColor,
            ),
          ),
          ...controller.listadoItemDrawer.map(
            (e) => _DrawerItem(
              titulo: e.titulo,
              icon: e.icon,
            ),
          ),
        ],
      ),
    );
  }
}

class _DrawerItem extends GetView<HomeFarmaciaController> {
  final String titulo;
  final IconData icon;

  const _DrawerItem({@required this.titulo, @required this.icon});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          dense: false,
          title: Text(
            titulo,
            style: AppFonts.primaryFont.copyWith(
              fontSize: 15,
            ),
          ),
          trailing: IconButton(
            icon: FaIcon(
              icon,
              size: 20,
              color: AppColors.secondaryColor,
            ),
            onPressed: () => controller.tapDrawerItem(titulo),
          ),
          onTap: () {
            controller.tapDrawerItem(titulo);
          },
        ),
        Divider()
      ],
    );
  }
}
