import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

import '../../../../config/themes/fonts.dart';
import '../homeFarmacia_controller.dart';

class AppBarFarmacia extends GetView<HomeFarmaciaController>
    with PreferredSizeWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: IconButton(
        icon: FaIcon(
          FontAwesomeIcons.bars,
        ),
        onPressed: controller.abrir,
      ),
      centerTitle: true,
      title: Text(
        'Dr. Click - Aliados',
        style: AppFonts.primaryFont.copyWith(
          fontSize: 18,
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(AppBar().preferredSize.height);
}
