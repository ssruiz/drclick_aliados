import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'homeFarmacia_controller.dart';
import 'widgets_locales/app_bar_farmacia_home.dart';
import 'widgets_locales/drawer_farmacia.dart';
import 'widgets_locales/home_grid_farmacia.dart';

class HomeFarmaciaPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeFarmaciaController>(
      builder: (_) {
        return SafeArea(
          child: Scaffold(
            key: _.drawer,
            appBar: AppBarFarmacia(),
            body: GridHomeFarmacia(),
            drawer: DrawerHomeFarmacia(),
          ),
        );
      },
    );
  }
}
