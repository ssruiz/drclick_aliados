import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../../config/routes/navigator.dart';
import '../../../config/routes/routes.dart';
import '../../../config/themes/colors.dart';
import '../../../core/utils/notifications/notificacion_service.dart';
import '../../../core/utils/notifications/notifications_keys.dart';
import '../../shared/global_widgets/yes_no_dialog.dart';
import '../domain/usecases/logout_use_case.dart';

class HomeFarmaciaController extends GetxController {
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  final nav = Get.find<NavigatorController>();
  final noti = Get.find<NotificationService>();
  GlobalKey get drawer => _drawerKey;

  List<_GridItem> listadoUi = [
    _GridItem(
      titulo: 'Pacientes',
      color: AppColors.secondaryColor,
      icon: FontAwesomeIcons.users,
    ),
    _GridItem(
      titulo: 'Recetas',
      color: AppColors.secondaryColor,
      icon: FontAwesomeIcons.bookMedical,
    ),
  ];

  List<_DrawerItem> listadoItemDrawer = [
    _DrawerItem(
      titulo: 'Inicio',
      icon: FontAwesomeIcons.houseUser,
    ),
    _DrawerItem(
      titulo: 'Cerrar Sesión',
      icon: FontAwesomeIcons.signOutAlt,
    ),
  ];

  abrir() {
    _drawerKey.currentState.openDrawer();
  }

  tapDrawerItem(String item) async {
    switch (item) {
      case 'Inicio':
        nav.goToAndClean(AppRoutes.HomeRouteFarmacia);
        break;
      case 'Cerrar Sesión':
        final dial = await DialogoSiNo()
            .abrirDialogoSiNo('Cerrar Sesión', '¿Terminar la sesión actual?');

        if (dial == 1) {
          print('Saliendo');
          final CerrarSesion cerrarSesion = Get.find<CerrarSesion>();
          final res = cerrarSesion();
          res.fold(
            (l) => noti.mostrarSnackBar(
              color: NotiKey.ERROR,
              mensaje: 'No se pudo terminar la sesión. Intente de vuelta',
              titulo: 'Cerrar Sesión',
            ),
            (r) => {
              nav.goToAndClean(AppRoutes.LoginRoute),
            },
          );
        } else
          print('Cancelado');

        break;
      default:
    }
  }

  tapGridItem(String item) async {
    switch (item) {
      case 'Pacientes':
        nav.goTo(AppRoutes.PacientesRoute);
        break;
      case 'Recetas':
        nav.goTo(AppRoutes.RecetasRoute);
        break;
      default:
    }
  }
}

class _GridItem {
  final String titulo;
  final Color color;
  final IconData icon;

  _GridItem({@required this.titulo, @required this.color, @required this.icon});
}

class _DrawerItem {
  final String titulo;
  final IconData icon;

  _DrawerItem({@required this.titulo, @required this.icon});
}
