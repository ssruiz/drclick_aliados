import 'package:meta/meta.dart';

import '../../../../core/authentication/domain/usecases/delete_auth_token.dart';

abstract class HomeFarmaciaLocalDataSource {
  /// Cierra la sesión del usuario y elimina el [TokenModel] del shared preferences
  ///
  /// Throws [CacheExpcetion] si ocurre un error
  bool cerrarSesion();
}

class HomeFarmaciaLocalDataSourceImpl implements HomeFarmaciaLocalDataSource {
  final DeleteAuthToken deleteAuthToken;

  HomeFarmaciaLocalDataSourceImpl({@required this.deleteAuthToken});

  bool cerrarSesion() {
    deleteAuthToken();
    return true;
  }
}
