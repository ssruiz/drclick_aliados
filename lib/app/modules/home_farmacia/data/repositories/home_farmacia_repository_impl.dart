import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/errors/failures.dart';
import '../../domain/repository/home_farmacia_repository.dart';
import '../datasources/local_data_source.dart';

class HomeFarmaciaRepositoryImpl implements HomeFarmaciaRepository {
  final HomeFarmaciaLocalDataSource dataSource;

  HomeFarmaciaRepositoryImpl({@required this.dataSource});

  @override
  Either<Failure, bool> logout() {
    try {
      return Right(dataSource.cerrarSesion());
    } catch (e) {
      return Left(CacheFailure(mensaje: e.toString()));
    }
  }
}
