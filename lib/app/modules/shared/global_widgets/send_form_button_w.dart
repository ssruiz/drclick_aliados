import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:reactive_forms/reactive_forms.dart';

import '../../../config/themes/colors.dart';
import '../../login/ui/login_controller.dart';

class FormularioSendButton extends StatelessWidget {
  const FormularioSendButton({
    @required this.controller,
    @required this.form,
    @required this.funcion,
    @required this.height,
    @required this.width,
    @required this.ignore,
  });

  final LoginController controller;
  final FormGroup form;
  final Function funcion;
  final double height;
  final double width;
  final RxBool ignore;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: AppColors.primaryColor,
          onSurface: Colors.blue,
          minimumSize: Size(
            width,
            height,
          ),
        ),
        child: _LoginButtonChild(
          controller: controller,
          ignore: ignore,
        ),
        onPressed: form.valid ? funcion : null,
      ),
    );
  }
}

class _LoginButtonChild extends StatelessWidget {
  const _LoginButtonChild({
    @required this.controller,
    @required this.ignore,
  });

  final LoginController controller;
  final RxBool ignore;
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      child: Obx(
        () => ignore.value
            ? CircularProgressIndicator(
                backgroundColor: Colors.white,
              )
            : Text(
                'Ingresar',
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                ),
              ),
      ),
    );
  }
}
