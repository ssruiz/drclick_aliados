import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../config/themes/colors.dart';
import '../../../config/themes/fonts.dart';

class DialogoSiNo {
  Future<int> abrirDialogoSiNo(String titulo, String mensaje) async =>
      await Get.dialog<int>(
        _DialogSiNo(
          titulo: titulo,
          mensaje: mensaje,
        ),
        useSafeArea: true,
        barrierDismissible: false,
      );
}

class _DialogSiNo extends StatelessWidget {
  final String titulo;
  final String mensaje;

  const _DialogSiNo({@required this.titulo, @required this.mensaje});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Dialog(
        clipBehavior: Clip.hardEdge,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        child: _StackDialog(
          mensaje: mensaje,
          titulo: titulo,
        ),
      ),
    );
  }
}

class _StackDialog extends StatelessWidget {
  final String titulo;
  final String mensaje;

  const _StackDialog({@required this.titulo, @required this.mensaje});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            left: 20,
            top: 35.0,
            right: 20,
            bottom: 20.0,
          ),
          margin: EdgeInsets.only(top: 45.0),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black,
                offset: Offset(0, 10),
                blurRadius: 10,
              ),
            ],
          ),
          child: _Formulario(
            mensaje: mensaje,
            titulo: titulo,
          ),
        ),
        Positioned(
          left: 20,
          right: 20,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 45,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(45)),
              child: Image.asset(
                "assets/logo3.png",
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class _Formulario extends StatelessWidget {
  final String titulo;
  final String mensaje;

  const _Formulario({@required this.titulo, @required this.mensaje});

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(minHeight: 150, maxHeight: 200),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            titulo,
            style: AppFonts.primaryFont
                .copyWith(fontSize: 22, fontWeight: FontWeight.w500),
          ),
          SizedBox(
            height: 5,
          ),
          Divider(
            height: 1,
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            mensaje,
            style: AppFonts.primaryFont
                .copyWith(fontSize: 18, fontWeight: FontWeight.w500),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ConstrainedBox(
                constraints: BoxConstraints(minWidth: 90, maxWidth: 150),
                child: OutlinedButton(
                  child: Text('Cancelar'),
                  onPressed: () => Get.back(result: 0),
                ),
              ),
              ConstrainedBox(
                constraints: BoxConstraints(minWidth: 90, maxWidth: 150),
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                      AppColors.secondaryColor,
                    ),
                  ),
                  child: Text('Salir'),
                  onPressed: () => Get.back(result: 1),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
