import 'package:flutter/material.dart';

import '../../../config/themes/fonts.dart';

class AppBarSeccion extends StatelessWidget with PreferredSizeWidget {
  final String titulo;

  const AppBarSeccion({@required this.titulo});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        titulo,
        style: AppFonts.primaryFont.copyWith(
          fontSize: 18,
        ),
      ),
      centerTitle: true,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(AppBar().preferredSize.height);
}
