import 'dart:convert';

import 'historico_paciente_model.dart';
import 'tipo_prescripcion.dart';
import 'package:meta/meta.dart';

import '../../domain/entities/paciente_receta_entity.dart';

List<PacienteRecetaModel> pacienteRecetaModelFromJson(List<dynamic> str) {
  final p = str.map((e) => PacienteRecetaModel.fromJson(e)).toList();
  print(p.length);
  return p;
}
// List<PacienteRecetaModel>.from(
//     json.decode(str).map((x) => PacienteRecetaModel.fromJson(x)));

class PacienteRecetaModel extends PacienteRecetaEntity {
  PacienteRecetaModel({
    @required idHistoricoPacienteDetalle,
    @required historicoPaciente,
    @required tipoPreescripcionMedica,
    @required descripcion,
    @required String adjunto,
    @required valido,
    @required recetaDetalles,
    @required String codigoBarra,
    @required controlado,
    @required observacion,
  }) : super(
          adjunto: adjunto,
          codigoBarra: codigoBarra,
          controlado: controlado,
          descripcion: descripcion,
          historicoPaciente: historicoPaciente,
          idHistoricoPacienteDetalle: idHistoricoPacienteDetalle,
          observacion: observacion,
          recetaDetalles: recetaDetalles,
          tipoPreescripcionMedica: tipoPreescripcionMedica,
          valido: valido,
        );

  factory PacienteRecetaModel.fromJson(Map<String, dynamic> json) =>
      PacienteRecetaModel(
        idHistoricoPacienteDetalle: json["idHistoricoPacienteDetalle"],
        historicoPaciente:
            HistoricoPacienteModel.fromJson(json["historicoPaciente"]),
        tipoPreescripcionMedica: TipoPreescripcionMedicaModel.fromJson(
          json["tipoPreescripcionMedica"],
        ),
        descripcion: json["descripcion"],
        adjunto: json["adjunto"],
        valido: json["valido"],
        recetaDetalles: json["recetaDetalles"],
        codigoBarra: json["codigoBarra"],
        controlado: json["controlado"],
        observacion: json["observacion"],
      );

  Map<String, dynamic> toJson() => {
        "idHistoricoPacienteDetalle": idHistoricoPacienteDetalle,
        "historicoPaciente": HistoricoPacienteModel.toJson(historicoPaciente),
        "tipoPreescripcionMedica":
            TipoPreescripcionMedicaModel.toJson(tipoPreescripcionMedica),
        "descripcion": descripcion,
        "adjunto": adjunto,
        "valido": valido,
        "recetaDetalles": recetaDetalles,
        "codigoBarra": codigoBarra,
        "controlado": controlado,
        "observacion": observacion,
      };
}
