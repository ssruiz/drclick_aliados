import '../../domain/entities/tipo_preescripcion.dart';
import 'package:meta/meta.dart';

class TipoPreescripcionMedicaModel extends TipoPreescripcionMedicaEntity {
  TipoPreescripcionMedicaModel({
    @required idTipoPreescripcionMedica,
    @required descripcion,
  }) : super(
          descripcion: descripcion,
          idTipoPreescripcionMedica: idTipoPreescripcionMedica,
        );

  factory TipoPreescripcionMedicaModel.fromJson(Map<String, dynamic> json) =>
      TipoPreescripcionMedicaModel(
        idTipoPreescripcionMedica: json["idTipoPreescripcionMedica"],
        descripcion: json["descripcion"],
      );

  static Map<String, dynamic> toJson(TipoPreescripcionMedicaEntity tp) => {
        "idTipoPreescripcionMedica": tp.idTipoPreescripcionMedica,
        "descripcion": tp.descripcion,
      };
}
