import 'package:meta/meta.dart';

import '../../../login/data/models/persona_model.dart';
import '../../domain/entities/medico_entity.dart';

class MedicoModel extends MedicoEntity {
  MedicoModel({
    @required idMedico,
    @required persona,
    @required img,
  }) : super(
          idMedico: idMedico,
          img: img,
          persona: persona,
        );

  factory MedicoModel.fromJson(Map<String, dynamic> json) => MedicoModel(
        idMedico: json["idMedico"],
        persona: PersonaModel.fromJson(json["persona"]),
        img: json["img"],
      );

  static Map<String, dynamic> toJson(MedicoEntity m) => {
        "idMedico": m.idMedico,
        "persona": {
          "idPersona": m.persona.idPersona,
          "nrodoc": m.persona.nrodoc,
          "nombreapellido": m.persona.nombreapellido,
          "correo": m.persona.correo,
          "celular": m.persona.celular,
          "sexo": m.persona.sexo,
          "fechaNacimiento": m.persona.fechaNacimiento,
        },
        "img": m.img,
      };
}
