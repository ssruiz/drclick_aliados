import 'package:meta/meta.dart';

import '../../domain/entities/consulta_medica_entity.dart';
import 'medico_model.dart';

class ConsultaMedicaModel extends ConsultaMedicaEntity {
  ConsultaMedicaModel({
    @required idConsultaMedica,
    @required descripcion,
    @required medico,
    @required fechaInicio,
  }) : super(
          descripcion: descripcion,
          fechaInicio: fechaInicio,
          idConsultaMedica: idConsultaMedica,
          medico: medico,
        );

  factory ConsultaMedicaModel.fromJson(Map<String, dynamic> json) =>
      ConsultaMedicaModel(
        idConsultaMedica: json["idConsultaMedica"],
        descripcion: json["descripcion"],
        medico: MedicoModel.fromJson(json["medico"]),
        fechaInicio: json["fechaInicio"],
      );

  static Map<String, dynamic> toJson(ConsultaMedicaEntity cm) => {
        "idConsultaMedica": cm.idConsultaMedica,
        "descripcion": cm.descripcion,
        "medico": MedicoModel.toJson(cm.medico),
        "fechaInicio": cm.fechaInicio,
      };
}
