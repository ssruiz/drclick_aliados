import 'package:meta/meta.dart';

import '../../domain/entities/historico_paciente_entity.dart';
import 'consulta_medica_model.dart';

class HistoricoPacienteModel extends HistoricoPacienteEntity {
  HistoricoPacienteModel({
    @required idHistoricoPaciente,
    @required consultaMedica,
  }) : super(
          consultaMedica: consultaMedica,
          idHistoricoPaciente: idHistoricoPaciente,
        );

  factory HistoricoPacienteModel.fromJson(Map<String, dynamic> json) =>
      HistoricoPacienteModel(
        idHistoricoPaciente: json["idHistoricoPaciente"],
        consultaMedica: ConsultaMedicaModel.fromJson(json["consultaMedica"]),
      );

  static Map<String, dynamic> toJson(HistoricoPacienteModel hp) => {
        "idHistoricoPaciente": hp.idHistoricoPaciente,
        "consultaMedica": ConsultaMedicaModel.toJson(hp.consultaMedica),
      };
}
