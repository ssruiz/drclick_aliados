import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/errors/failures.dart';
import '../../domain/entities/paciente_receta_entity.dart';
import '../../domain/repository/recetas_repository.dart';
import '../datasources/receta_local_data_source.dart';
import '../datasources/receta_remote_data_source.dart';

class RecetaRepositoryImpl implements RecetasRepository {
  final RecetaRemoteDataSource remoteDataSource;
  final RecetaLocalDataSource localDataSource;

  RecetaRepositoryImpl(
      {@required this.remoteDataSource, @required this.localDataSource});

  @override
  Future<Either<Failure, List<List<PacienteRecetaEntity>>>> buscarRecetaCi(
      String ci) async {
    try {
      return Right(await remoteDataSource.buscarRecetaCi(ci));
    } catch (e) {
      print(e);
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, PacienteRecetaEntity>> buscarRecetaCodigo(
      String codigo) async {
    try {
      return Right(await remoteDataSource.buscarRecetaCodigo(codigo));
    } catch (e) {
      return Left(ServerFailure());
    }
  }

  @override
  Future<Either<Failure, String>> scanCodigo() async {
    try {
      return Right(await localDataSource.scanCodigo());
    } catch (e) {
      return Left(ScanFailure());
    }
  }

  @override
  Future<Either<Failure, bool>> verificarReceta(int codigo) async {
    // TODO: implement verificarReceta
    try {
      return Right(await remoteDataSource.verificarReceta(codigo));
    } catch (e) {
      return Left(ServerFailure(mensaje: e.toString()));
    }
  }
}
