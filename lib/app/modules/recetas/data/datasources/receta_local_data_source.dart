import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

abstract class RecetaLocalDataSource {
  /// Escanea el codigo a través de la camara y devuelve un [String]
  ///
  /// En caso de un error lanza un [ScanException] si ocurre algún error
  Future<String> scanCodigo();
}

class RecetaLocalDataSourceImpl implements RecetaLocalDataSource {
  @override
  Future<String> scanCodigo() {
    return FlutterBarcodeScanner.scanBarcode(
      "#ff6666",
      "Cancelar",
      false,
      ScanMode.DEFAULT,
    );
  }
}
