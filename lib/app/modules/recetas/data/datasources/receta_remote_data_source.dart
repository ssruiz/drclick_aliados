import 'package:drclick_aliados/app/config/utils/constants.dart';
import 'package:drclick_aliados/app/core/shared/errors/failures.dart';
import 'package:drclick_aliados/app/core/utils/logger/logger_service.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/dio/dio_config.dart';
import '../models/receta_model.dart';

abstract class RecetaRemoteDataSource {
  /// Escanea el codigo a través de la camara y devuelve un [String]
  ///
  /// En caso de un error lanza un [ServerException] si ocurre algún error
  Future<List<List<PacienteRecetaModel>>> buscarRecetaCi(String ci);
  Future<PacienteRecetaModel> buscarRecetaCodigo(String ci);
  Future<bool> verificarReceta(int codigo);
}

class RecetaRemoteDataSourceImpl implements RecetaRemoteDataSource {
  final DioService dataSource;

  RecetaRemoteDataSourceImpl({@required this.dataSource});

  @override
  Future<List<List<PacienteRecetaModel>>> buscarRecetaCi(String ci) async {
    final res = await dataSource.client.get(
      AppConstants.API_URL + 'public/recetas/persona/CI/$ci',
    );
    // final logger = Get.find<LoggerService>().logger;
    // logger.d(res.data);
    //final Map<String, dynamic> data = res.data[1][0];

    //logger.d(data);
    List<PacienteRecetaModel> lista1 = pacienteRecetaModelFromJson(res.data[0]);

    List<PacienteRecetaModel> lista2 = pacienteRecetaModelFromJson(res.data[1]);
    //logger.d(lista1);
    // logger.d(lista2.length);
    // logger.d(lista1.length);
    return [lista1, lista2];
  }

  @override
  Future<PacienteRecetaModel> buscarRecetaCodigo(String ci) async {
    return null;
  }

  @override
  Future<bool> verificarReceta(int codigo) async {
    // TODO: implement verificarReceta. Falta URL
    final res = await dataSource.client.put(
      AppConstants.API_URL + 'urlverificarreceta/$codigo',
    );

    if (res.statusCode == 200) return Future.value(true);

    throw ServerFailure();
  }
}
