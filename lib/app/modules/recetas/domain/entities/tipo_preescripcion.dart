import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class TipoPreescripcionMedicaEntity extends Equatable {
  final int idTipoPreescripcionMedica;
  final String descripcion;

  TipoPreescripcionMedicaEntity({
    @required this.idTipoPreescripcionMedica,
    @required this.descripcion,
  });

  @override
  List<Object> get props => [idTipoPreescripcionMedica, descripcion];
}
