import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'consulta_medica_entity.dart';

class HistoricoPacienteEntity extends Equatable {
  final int idHistoricoPaciente;
  final ConsultaMedicaEntity consultaMedica;

  HistoricoPacienteEntity({
    @required this.idHistoricoPaciente,
    @required this.consultaMedica,
  });

  @override
  List<Object> get props => [idHistoricoPaciente, consultaMedica];
}
