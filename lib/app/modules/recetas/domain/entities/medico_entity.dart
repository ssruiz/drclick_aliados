import '../../../login/domain/entities/persona_entity.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class MedicoEntity extends Equatable {
  final int idMedico;
  final PersonaEntity persona;
  final String img;

  MedicoEntity({
    @required this.idMedico,
    @required this.persona,
    @required this.img,
  });

  @override
  List<Object> get props => [idMedico, persona, img];
}
