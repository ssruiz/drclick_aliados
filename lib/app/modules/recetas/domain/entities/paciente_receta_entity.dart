import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'historico_paciente_entity.dart';
import 'tipo_preescripcion.dart';

class PacienteRecetaEntity extends Equatable {
  final int idHistoricoPacienteDetalle;
  final HistoricoPacienteEntity historicoPaciente;
  final TipoPreescripcionMedicaEntity tipoPreescripcionMedica;
  final String descripcion;
  final String adjunto;
  final bool valido;
  final dynamic recetaDetalles;
  final String codigoBarra;
  final bool controlado;
  final dynamic observacion;

  PacienteRecetaEntity({
    @required this.idHistoricoPacienteDetalle,
    @required this.historicoPaciente,
    @required this.tipoPreescripcionMedica,
    @required this.descripcion,
    @required this.adjunto,
    @required this.valido,
    @required this.recetaDetalles,
    @required this.codigoBarra,
    @required this.controlado,
    @required this.observacion,
  });

  @override
  List<Object> get props => [
        idHistoricoPacienteDetalle,
        historicoPaciente,
        tipoPreescripcionMedica,
        descripcion,
        adjunto,
        valido,
        recetaDetalles,
        codigoBarra,
        controlado,
        observacion,
      ];
}
