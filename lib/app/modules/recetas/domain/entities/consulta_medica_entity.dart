import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'medico_entity.dart';

class ConsultaMedicaEntity extends Equatable {
  final int idConsultaMedica;
  final String descripcion;
  final MedicoEntity medico;
  final String fechaInicio;

  ConsultaMedicaEntity({
    @required this.idConsultaMedica,
    @required this.descripcion,
    @required this.medico,
    @required this.fechaInicio,
  });

  @override
  List<Object> get props => [
        idConsultaMedica,
        descripcion,
        medico,
        fechaInicio,
      ];
}
