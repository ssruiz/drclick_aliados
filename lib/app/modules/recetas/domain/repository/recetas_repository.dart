import 'package:dartz/dartz.dart';

import '../../../../core/shared/errors/failures.dart';
import '../entities/paciente_receta_entity.dart';

abstract class RecetasRepository {
  Future<Either<Failure, String>> scanCodigo();
  Future<Either<Failure, List<List<PacienteRecetaEntity>>>> buscarRecetaCi(
      String ci);
  Future<Either<Failure, PacienteRecetaEntity>> buscarRecetaCodigo(
      String codigo);

  Future<Either<Failure, bool>> verificarReceta(int codigo);
}
