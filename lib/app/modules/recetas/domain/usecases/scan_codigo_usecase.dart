import 'package:dartz/dartz.dart';
import 'package:drclick_aliados/app/core/shared/errors/failures.dart';
import 'package:drclick_aliados/app/core/shared/usecases/usecase.dart';
import 'package:meta/meta.dart';

import '../repository/recetas_repository.dart';

class ScanCodigo implements UseCase<String> {
  final RecetasRepository repository;

  ScanCodigo({@required this.repository});

  @override
  Future<Either<Failure, String>> call() => repository.scanCodigo();
}
