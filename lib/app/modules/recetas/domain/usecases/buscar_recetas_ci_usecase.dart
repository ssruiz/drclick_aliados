import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/errors/failures.dart';
import '../../../../core/shared/usecases/usecase_params.dart';
import '../entities/paciente_receta_entity.dart';
import '../repository/recetas_repository.dart';

class BuscarRecetasCi
    implements UseCaseWithParams<List<List<PacienteRecetaEntity>>, String> {
  final RecetasRepository repository;

  BuscarRecetasCi({@required this.repository});

  @override
  Future<Either<Failure, List<List<PacienteRecetaEntity>>>> call(String ci) =>
      repository.buscarRecetaCi(ci);
}
