import 'package:dartz/dartz.dart';
import 'package:drclick_aliados/app/core/shared/errors/failures.dart';
import 'package:drclick_aliados/app/core/shared/usecases/usecase.dart';
import 'package:drclick_aliados/app/core/shared/usecases/usecase_params.dart';
import 'package:drclick_aliados/app/modules/recetas/domain/repository/recetas_repository.dart';
import 'package:meta/meta.dart';

class VerificarReceta implements UseCaseWithParams<bool, int> {
  final RecetasRepository repository;

  VerificarReceta({@required this.repository});

  @override
  Future<Either<Failure, bool>> call(int codigo) =>
      repository.verificarReceta(codigo);
}
