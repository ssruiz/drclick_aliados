import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';

import '../../../../core/shared/errors/failures.dart';
import '../../../../core/shared/usecases/usecase_params.dart';
import '../entities/paciente_receta_entity.dart';
import '../repository/recetas_repository.dart';

class BuscarRecetasCodigo
    implements UseCaseWithParams<PacienteRecetaEntity, String> {
  final RecetasRepository repository;

  BuscarRecetasCodigo({@required this.repository});

  @override
  Future<Either<Failure, PacienteRecetaEntity>> call(String ci) =>
      repository.buscarRecetaCodigo(ci);
}
