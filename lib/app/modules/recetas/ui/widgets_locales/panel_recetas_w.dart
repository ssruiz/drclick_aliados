import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'expansion_item_w.dart';

class PanelRecetas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        ExpansionItem(
          titulo: 'Disponibles',
          listado: 1,
          icon: FontAwesomeIcons.layerGroup,
        ),
        ExpansionItem(
          titulo: 'Historico',
          listado: 2,
          icon: FontAwesomeIcons.history,
        ),
      ],
    );
  }
}
