import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../../../../config/themes/colors.dart';
import '../../../../config/themes/fonts.dart';
import '../recetas_controller.dart';

class ExpansionItem extends GetView<RecetasController> {
  ExpansionItem({
    @required this.titulo,
    @required this.listado,
    @required this.icon,
  });

  final String titulo;
  final int listado;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    final listadoItem = listado == 1 ? controller.lista1 : controller.lista2;

    return Obx(
      () => ExpansionTile(
        leading: FaIcon(icon, color: AppColors.primaryColor),
        title: Text(
          titulo,
          style: AppFonts.primaryFont.copyWith(
            fontSize: 18,
            color: AppColors.primaryColor,
          ),
        ),
        children: <Widget>[
          if (listadoItem.length == 0)
            ListTile(
              title: Text('No se encontraron registros'),
            )
          else if (listado == 1)
            ...listadoDisponibles()
          else
            ...listadoHistorico(),
        ],
      ),
    );
  }

  List<Widget> listadoHistorico() => controller.lista2
      .map<Widget>(
        (element) => ListTile(
          dense: true,
          // leading: Column(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: [
          //     FaIcon(
          //       FontAwesomeIcons.fileMedical,
          //       color: AppColors.secondaryColor,
          //     ),
          //   ],
          // ),
          title: Text(
            element.historicoPaciente.consultaMedica.descripcion,
          ),
          subtitle: Text(element.descripcion),
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconButton(
                icon: FaIcon(
                  FontAwesomeIcons.filePdf,
                  color: Colors.red,
                ),
                onPressed: () => controller.abrirPdf(element.adjunto),
              ),
              SizedBox(
                height: 10,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: AppColors.primaryColor,
                  onPrimary: Colors.white,
                  onSurface: Colors.grey,
                ),
                child: Text('Verificar'),
                onPressed: () => controller
                    .verificarReceta(element.idHistoricoPacienteDetalle),
              )
            ],
          ),
        ),
      )
      .toList();

  List<Widget> listadoDisponibles() => controller.lista1
      .map<Widget>(
        (element) => ListTile(
          dense: true,
          leading:
              Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            FaIcon(
              FontAwesomeIcons.fileMedical,
              color: AppColors.secondaryColor,
            ),
          ]),
          title: Text(
            element.historicoPaciente.consultaMedica.descripcion,
          ),
          subtitle: Text(element.descripcion),
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconButton(
                icon: FaIcon(
                  FontAwesomeIcons.filePdf,
                  color: Colors.red,
                ),
                onPressed: () => controller.abrirPdf(element.adjunto),
              ),
            ],
          ),
        ),
      )
      .toList();
}
