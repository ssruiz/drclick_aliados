import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:reactive_forms/reactive_forms.dart';

import '../../../../config/themes/colors.dart';
import '../../../../config/themes/fonts.dart';
import '../recetas_controller.dart';

class BusquedaRecetaWidget extends GetView<RecetasController> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: ReactiveForm(
            formGroup: controller.filtroBusquedaForm,
            child: Container(
              child: _InputWidget(
                controlName: 'busquedaFiltro',
                labelText: 'Buscar Receta',
                hintText: 'Ingrese el ci del paciente',
                prefixIcon: FontAwesomeIcons.search,
              ),
            ),
          ),
        ),
        IconButton(
          icon: FaIcon(
            FontAwesomeIcons.barcode,
            color: AppColors.secondaryColor,
            size: 15,
          ),
          onPressed: controller.scanearReceta,
        ),
      ],
    );
  }
}

class _InputWidget extends GetView<RecetasController> {
  const _InputWidget({
    @required this.controlName,
    @required this.hintText,
    @required this.prefixIcon,
    @required this.labelText,
  });

  final String controlName;
  final String hintText;
  final String labelText;
  final IconData prefixIcon;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ReactiveTextField(
        formControlName: controlName,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          isDense: true,
          suffixIcon: IconButton(
            icon: FaIcon(
              prefixIcon,
              color: AppColors.secondaryColor,
              size: 15,
            ),
            onPressed: controller.buscarReceta,
          ),
          hintText: hintText,
          labelText: labelText,
          labelStyle: AppFonts.primaryFont.copyWith(
            fontSize: 15,
          ),
          hintStyle: AppFonts.primaryFont.copyWith(
            fontSize: 18,
          ),
        ),
        style: AppFonts.primaryFont.copyWith(fontSize: 18),
      ),
    );
  }
}
