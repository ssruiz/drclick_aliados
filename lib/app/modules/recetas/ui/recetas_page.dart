import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../shared/global_widgets/app_bar_secciones_w.dart';
import '../../shared/global_widgets/buscando_progress_w.dart';
import 'recetas_controller.dart';
import 'widgets_locales/busqueda_codigo_receta_w.dart';
import 'widgets_locales/panel_recetas_w.dart';

class RecetasPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<RecetasController>(
      builder: (_) {
        return SafeArea(
          child: Scaffold(
            appBar: AppBarSeccion(
              titulo: 'Recetas',
            ),
            body: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  BusquedaRecetaWidget(),
                  SizedBox(
                    height: 20,
                  ),
                  BuscandoProgressWidget(buscando: _.buscando),
                  Obx(
                    () {
                      if (_.busquedaActual == '1')
                        return Expanded(
                          child: PanelRecetas(),
                        );
                      else if (_.busquedaActual == '2')
                        return Expanded(
                          child: PanelRecetas(),
                        );
                      else
                        return Container();
                    },
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
