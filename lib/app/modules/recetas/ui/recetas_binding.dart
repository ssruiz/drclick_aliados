import 'package:get/get.dart';

import 'recetas_controller.dart';

class RecetasBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => RecetasController());
  }
}
