import 'package:drclick_aliados/app/config/routes/navigator.dart';
import 'package:drclick_aliados/app/config/routes/routes.dart';
import 'package:drclick_aliados/app/modules/recetas/domain/entities/paciente_receta_entity.dart';
import 'package:drclick_aliados/app/modules/recetas/domain/usecases/buscar_recetas_ci_usecase.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:get/get.dart';
import 'package:reactive_forms/reactive_forms.dart';

import '../../../core/utils/logger/logger_service.dart';

class RecetasController extends GetxController {
  RxBool _buscando = false.obs;
  RxString _busquedaActual = ''.obs;
  RxString barCode = ''.obs;
  final logger = Get.find<LoggerService>().logger;

  RxBool get buscando => _buscando;
  String get busquedaActual => _busquedaActual.value;

  RxList<PacienteRecetaEntity> lista1 = <PacienteRecetaEntity>[].obs;
  RxList<PacienteRecetaEntity> lista2 = <PacienteRecetaEntity>[].obs;

  final FormGroup filtroBusquedaForm = FormGroup({
    'busquedaFiltro': FormControl(
      value: '',
      // validators: [Validators.minLength(7)],
    ),
  });

  void buscarReceta() async {
    _buscando.value = true;
    final ci = filtroBusquedaForm.control('busquedaFiltro').value;

    final BuscarRecetasCi buscarRecetasCi = Get.find<BuscarRecetasCi>();
    final res = await buscarRecetasCi(ci);
    res.fold(
      (l) {
        lista1.assignAll([]);
        lista2.assignAll([]);
        _buscando.value = false;
      },
      (r) {
        print('Success');
        lista1.assignAll(r[0]);
        lista2.assignAll(r[1]);
        _buscando.value = false;
        _busquedaActual.value = '1';
        filtroBusquedaForm.unfocus();
      },
    );
  }

  void scanearReceta() async {
    String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
      "#ff6666",
      "Cancelar",
      false,
      ScanMode.DEFAULT,
    );
    _busquedaActual.value = '2';
    //TODO implementar el scaneo
    final log = Get.find<LoggerService>();
    log.logger.d(barcodeScanRes);
  }

  void abrirPdf(String path) {
    final nav = Get.find<NavigatorController>();

    nav.goToPdf(AppRoutes.PdfRoute, path);
  }

  void verificarReceta(int idHistorico) async {
    //TODO implementar validación receta
  }
}
