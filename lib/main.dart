import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'app/config/routes/pages.dart';
import 'app/config/routes/routes.dart';
import 'app/config/themes/colors.dart';
import 'app/core/utils/dependencies/dependency_injection.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DependencyInjection.init();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then(
    (value) => runApp(
      MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Dr. CLick - Aliados',
      // rutas
      initialRoute: AppRoutes.SplashRoute,
      getPages: AppPages.pages,

      // idioma

      locale: Get.deviceLocale,
      //supportedLocales: [const Locale('es', 'PY')],
      // tema
      theme: Theme.of(context).copyWith(
        primaryColor: AppColors.primaryColor,
        textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
            primary: AppColors.primaryColor,
          ),
        ),
      ),
    );
  }
}
